import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'

import registerServiceWorker from './registerServiceWorker'

import { ThemeProvider } from 'styled-components'

import storage from './util/storage.js'

import Header from './components/Header.js'
import App from './components/App'
import Login from './components/Login'
import './index.css'
import themes from './themes.js'

const JWTExpired = token => token ? (new Date() / 1000 - JSON.parse(atob(token.split('.')[1])).exp) <= 0 : false

const initStorage = () => {
  if(storage.getItem('init') && storage.getItem('init') === "true") {
    return true
  } else {
    if(!storage.getItem('names')) storage.json.setItem('names', {})
    if(!storage.getItem('items')) storage.json.setItem('items', {})
    storage.setItem('init', true)
  }
}

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props =>
    JWTExpired(storage.getItem('jwt'))
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', from: props.location }} />
  } />
)

const load_theme = () => (storage.json.getItem(storage.getItem('user')) || {theme: 'default'}).theme

class Wrapper extends Component {
  constructor(props) {
    super(props)

    console.log(this.props)

    this.state = {
      theme: this.props.theme
    }

    storage.addEventListener('set', e => e.name === 'settings' ? this.setState({ theme: JSON.parse(e.newValue).theme }) : null)
  }

  render() {
    return (
      <Router>
        <ThemeProvider theme={themes[this.state.theme]}>
          <div>
            <Header />
            <Switch>
              <Route exact path="/" component={Login} />
              <Route exact path="/login" component={Login} />
              <PrivateRoute component={App} />
            </Switch>
          </div>
        </ThemeProvider>
      </Router>
    )
  }
}

initStorage()

ReactDOM.render(<Wrapper theme={load_theme()} />,
  document.getElementById('root')
)

registerServiceWorker()
