export default {
  default: {
    main: {
      bg: '#ffffff',
      color: '#000000'
    },
    Header: {
      bg: '#a2e6f9',
      color: '#124454',
      filter: 'none',
    },
    SideBar: {
      color: '#ffffff',
      bg1: '#47656d',
      bg2: '#37474F',
      bg3: '#47656d',
      bg4: '#3c565d',
      bg5: '#37474F',
    },
    Elements: {
      color: '#000000',
      bg1: '#ffffff',
      bg2: '#124454',
      border1: '#d1d1d1',
      border2: '#33C3F0',
    },
    Popup: {
      disabled: '#888888',
    },
    DayView: {
      border: '#dedede',
      reserved: 'hsl(193, 88%, 71%)',
      bg_hover: 'hsla(193, 88%, 71%, 0.5)'
    },
    WeekView: {
      border: '#dedede',
      reserved: 'hsl(193, 88%, 71%)',
      bg_hover: 'hsla(193, 88%, 71%, 0.3)'
    },
    MonthView: {

    },
    NavButton: {
      color: 'white',
      bg1: 'hsla(195, 65%, 20%, 1)',
      bg2: 'hsla(195, 65%, 25%, 1)',
    },
    InputField: {
      bg1: '#ffffff',
      border1: '#aaaaaa',
      border2: '#000000',
      color1: '#000000',
      color2: 'red'
    },
    db: {
      color: '#000000',
      border1: '#2185d0',
      border2: '#ececec',
      border3: '#999999',
      border4: '#ececec',
      bg1: '#f5f5f5',
      bg2: '#ffffff',
      bg3: '#ffffff',
    },

  },
  dark: {
    main: {
      bg: '#000000',
      color: '#ffffff'
    },
    Header: {
      bg: 'hsla(338, 78%, 38%, 1)',
      color: '#ffffff',
      filter: 'brightness(100)',
    },
    SideBar: {
      color: '#ffffff',
      bg1: 'hsla(338, 78%, 43%, 1)',
      bg2: 'hsla(338, 78%, 33%, 1)',
      bg3: 'hsla(338, 78%, 43%, 1)',
      bg4: 'hsla(338, 78%, 35%, 1)',
      bg5: 'hsla(338, 78%, 28%, 1)',
    },
    Elements: {
      color: '#ffffff',
      bg1: '#000000',
      bg2: 'hsla(338, 78%, 28%, 1)',
      border1: '#d1d1d1',
      border2: 'hsla(338, 78%, 48%, 1)',
    },
    Popup: {
      disabled: '#888888',
    },
    DayView: {
      border: '#dedede',
      reserved: 'hsla(338, 78%, 38%, 1)',
      bg_hover: 'rgba(162, 230, 249, 0.5)'
    },
    WeekView: {
      border: '#dedede',
      reserved: 'hsla(338, 78%, 38%, 1)',
      bg_hover: 'rgba(162, 230, 249, 0.5)'
    },
    MonthView: {

    },
    NavButton: {
      color: 'white',
      bg1: 'hsla(195, 65%, 20%, 1)',
      bg2: 'hsla(195, 65%, 25%, 1)',
    },
    InputField: {
      bg1: '#ffffff',
      border1: '#aaaaaa',
      border2: '#000000',
      color1: '#000000',
      color2: 'red'
    },
    db: {
      color: '#ffffff',
      border1: 'hsla(338, 78%, 48%, 1)',
      border2: '#222222',
      border3: '#666',
      border4: '#222222',
      bg1: '#090909',
      bg2: 'hsla(338, 78%, 28%, 1)',
      bg3: '#000000',
    }
  },
}
