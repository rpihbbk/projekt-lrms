import storage from './storage.js'
import fetchJSON from './fetchJSON.js'


let name = {
  running: [],
  promises: {}
}

let id = {
  running: [],
  promises: {}
}

// let item = {
//   running: [],
//   promises: {}
// }

const namelookup = (id) => {
  if(id === 1 || id === "1")
    return "root"

  let { running, promises}  = name

  if(!storage.getItem('names'))
    storage.json.setItem('names', {})

  const lookup = storage.json.getItem('names')

  if(lookup[id]) {
    return lookup[id]
  } else {
    console.log('[name] cache: ✗ (' + id + ')')
    if(running.indexOf(id) === -1) {
      console.log('[name] running: ✗', running)
      running.push(id)
      promises[id] = []
    } else {
      console.log('[name] running: ✔', running)
      return new Promise((resolve, reject) => {
        promises[id].push({
          resolve: resolve,
          reject: reject,
        })
      })
    }
    return fetchJSON(window.BACKEND + '/namelookup/' + id, null, 'GET')
      .then(obj => obj.username)
      .then(username => {
        promises[id] = promises[id]
          .map(promise => promise.resolve(username)).filter(x => false)
        return username
      })
      .then(username => {
        running = running.filter(x => x !== id)
        return username
      })
      .then(username => {
        lookup[id] = username
        storage.json.setItem('names', lookup)
        return username
      })
  }
}

const reverse = (obj, x) => Object.keys(obj)[Object.values(obj).indexOf(x)]

const idlookup = (name) => {
  if(name === 'root')
    return '1'

  let { running, promises}  = id

  if(!storage.getItem('names'))
    storage.json.setItem('names', {})

  const lookup = storage.json.getItem('names')

  if(reverse(lookup, name)) {
    return reverse(lookup, name)
  } else {
    console.log('[id] cache: ✗ (' + name + ')')
    if(running.indexOf(name) === -1) {
      console.log('[id] running: ✗', running)
      running.push(name)
      promises[name] = []
    } else {
      console.log('[id] running: ✔', running)
      return new Promise((resolve, reject) => {
        promises[name].push({
          resolve: resolve,
          reject: reject
        })
      })
    }
    return fetchJSON(window.BACKEND + '/idlookup/' + name, null, 'GET')
      .then(obj => obj.id)
      .then(id => {
        promises[name] = promises[name]
          .map(promise => promise.resolve(id)).filter(x => false)
        return id
      })
      .then(id => {
        running = running.filter(x => x !== name)
        return id
      })
      .then(id => {
        lookup[id] = name
        storage.json.setItem('names', lookup)
        return id
      })
  }
}

const itemlookup = (itemId) => {
  if(!storage.getItem('items'))
    storage.json.setItem('items', {})

  const lookup = storage.json.getItem('items')

  if(lookup[itemId]) {
    return lookup[itemId]
  } else {
    return fetchJSON(window.BACKEND + '/item/get/', {id: itemId}, 'POST')
      .then(obj => {
        lookup[itemId] = obj
        storage.json.setItem('items', lookup)
        return obj
      })
  }
}

export { namelookup, idlookup, itemlookup, reverse }
