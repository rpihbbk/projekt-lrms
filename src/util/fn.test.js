import fn from './fn.js'

it('fn flatten shallow flattens the array (non recursive)', () => {
  expect(fn.flatten([1, [2, [3, [4]]]])).toEqual([1, 2, [3, [4]]])
})

it('fn flattenDeep deep flattens the array (recursive)', () => {
  expect(fn.flattenDeep([1, [2, [3, [4]]]])).toEqual([1, 2, 3, 4])
})

it('fn mapObj returns a modified object with the same keys as the input', () => {
  expect(fn.mapObj({a: 1, b: 2}, (value, key) => value + 1)).toEqual({a: 2, b: 3})
})
