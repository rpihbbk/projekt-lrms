import dates from './dates.js'
/*
{
  "teacher": "1", yes
  "item": 1, yes
  "start": 2, yes
  "end": 2, yes
  "date": "2017-08-15T00:00:00.000Z", idk
  "room": 123, yes
  "title": "test numero unendlich vermutlich inzwischen", yes
  "recurring": false, idk
  "recurring_time": null, idk
  "_id": "FzfgZmJsC64ZJJ0X"
},

let hours = [...Array(12).keys()].map(j => ({
  reserved: false,
  person: '',
  room: undefined,
  title: '',
  item: undefined
}))

hours[1] = {
  reserved: true,
  person: 'BRA',
  room: 123,
  title: '',
  item: 1,
}
*/

const convertDay = input => {
  let hours = [...Array(12).keys()].map(j => ({
    reserved: false,
    person: '',
    title: '',
    room: undefined,
    item: undefined
  }))

  input.forEach(x => {
    let obj = {
      reserved: true,
      person: x.teacher,
      title: x.title,
      item: x.item,
      room: x.room,
      id: x._id,
      start: x.start,
      end: x.end,
      date: dates.stringifyDateInfoPadded(new Date(x.date), 0, 0)
    }
    for(let i = x.start-1;i<x.end;i++) {
      hours[i] = obj
    }
  })
  return hours
}

const convertWeek = input => {
  let hours = [...Array(12).keys()].map(j => [...Array(7).keys()].map(j => ({
    reserved: false,
    person: undefined,
    room: undefined,
    title: undefined,
    item: undefined,
  })))

  input.forEach(x => {
    let obj = {
      reserved: true,
      person: x.teacher,
      title: x.title,
      item: x.item,
      room: x.room,
      id: x._id,
      start: x.start,
      end: x.end,
      date: dates.stringifyDateInfoPadded(new Date(x.date), 0, 0)
    }

    // assign events to days of the week and then to hours (this should be just like in convertDay)
    for(let i = x.start-1;i<x.end;i++) {
      hours[i][dates.dayOfWeek(x.date)] = obj
    }
  })
  return hours
}

export { convertDay, convertWeek }
