const dates = {
  setTime: (date, hours=0, minutes=0, seconds=0, milliseconds=0) =>
    new Date(new Date(date).setHours(hours, minutes, seconds, milliseconds)),
  setTimeIgnoreTimezone: (date, hours=0, minutes=0, seconds=0, milliseconds=0) =>
    new Date(new Date(date).setHours(hours, minutes - date.getTimezoneOffset(), seconds, milliseconds)),
  compareWithoutTime: (date1, date2) =>
    (date1.setHours(0, 0, 0, 0) === date2.setHours(0, 0, 0, 0)),
  stringifyDateInfo: (date=new Date(Date.now()), offset1=0, offset2=0) =>
    dates._stringifyDateInfo(date, offset1, offset2),
  stringifyDateInfoPadded: (date=new Date(Date.now()), offset1=0, offset2=0) =>
    dates._stringifyDateInfoPadded(date, offset1, offset2),
  _stringifyDateInfo: (date, offset1=0, offset2=0) => {
    if(!date) return null
    let l_date = new Date(date.getFullYear(), date.getMonth() + offset1, date.getDate() + offset2)
    return `${l_date.getFullYear()}-${l_date.getMonth() + 1}-${l_date.getDate()}`
  },
  _stringifyDateInfoPadded: (date, offset1=0, offset2=0) => {
    if(!date) return null
    const paddingLeft  = (str, padding, length) => str.length < length ? padding.repeat(length - str.length) + str : str
  /*
    const paddingRight = (str, padding, length) => str.length < length ? str + padding.repeat(length - str.length) : str

    this is basically String.padStart(length, paddingStr) / String.padEnd(length, paddingStr)
    did not really know they existed or rather forgot that they exist

    paddingRight is commented out because it is not used at the moment and eslint

    paddingRight and paddingLeft will most likely move to their own file, as soon as there are more useful String manipulation functions
  */
    let l_date = new Date(date.getFullYear(), date.getMonth() + offset1, date.getDate() + offset2)
    return `${l_date.getFullYear()}-${paddingLeft(String(l_date.getMonth() + 1), '0', 2)}-${paddingLeft(String(l_date.getDate()), '0', 2)}`
  },
  parseDateInfo: (str) =>
    // str should be in ISO 8601 format (e.g. 2017-05-26) (short form without time)
    str === '' || str === undefined
      ? new Date(Date.now())
      : dates._parseDateInfo(str),
  _parseDateInfo: (str) => {
    if(!str) return null
    str = str.split('-')
    return new Date(Date.UTC(str[0], str[1]-1, str[2]))
  },
  _dayOfWeek: (date) => date.getDay() === 0 ? 6 : date.getDay() - 1,
  dayOfWeek: (date) => dates._dayOfWeek(date ? new Date(date): new Date()),
  calculateWeeks: (date) => {
    let lastDateOfMonth = new Date(date.getFullYear(), date.getMonth()+1, 0).getDate()
    let numberOfWeeks = Math.ceil((((new Date(date.getFullYear(), date.getMonth(), 1).getDay() + 6) % 7) + lastDateOfMonth) / 7)
    let weeks = []
    for(let j = 0; j < numberOfWeeks; j++) {
      weeks[j] = []
      for(let k = 0; k < 7; k++) {
        weeks[j][k] = 0
      }
    }
    let i = 0
    for(let k = 1; k < lastDateOfMonth + 1; k++) {
      let l_day = new Date(date.getFullYear(), date.getMonth(), k)
      let l_day_of_week = l_day.getDay() - 1 === -1 ? 6 : l_day.getDay() - 1

      weeks[i][l_day_of_week] = l_day
      if(l_day_of_week === 6)
        i++
    }
    return {
      lastDateOfMonth: lastDateOfMonth,
      numberOfWeeks: numberOfWeeks,
      weeks: weeks,
    }
  },
  months: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
  week_days_short: ["Mon", "Die", "Mit", "Don", "Fre", "Sam", "Son"]
}


export default dates
