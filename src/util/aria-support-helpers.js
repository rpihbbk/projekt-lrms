export default {
  spacebar: (e, cb) => e.charCode === 32 ? cb() : '',
  enter:    (e, cb) => e.charCode === 13 ? cb() : '',
  esc:      (e, cb) => e.charCode === 27 ? cb() : '',
}
