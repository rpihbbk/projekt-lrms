import clear from './commands/clear.js'
import help from './commands/help.js'
import man from './commands/man.js'
import adduser from './commands/user/adduser.js'
import deluser from './commands/user/deluser.js'
import moduser from './commands/user/moduser.js'
import listuser from './commands/user/listuser.js'
import additem from './commands/item/additem.js'
import delitem from './commands/item/delitem.js'
import moditem from './commands/item/moditem.js'
import listitem from './commands/item/listitem.js'
import addevent from './commands/event/addevent.js'
import delevent from './commands/event/delevent.js'
import modevent from './commands/event/modevent.js'
import listevent from './commands/event/listevent.js'

let _createArgsFromObj = (args) =>
  Object.keys(args).map(arg => isNaN(arg) ? '--' + arg + args[arg] : args[arg]).join(' ')

let _createFlagsFromObj = (flags) =>
  (Object.keys(flags).length === 0 ? '' : ' -') + Object.keys(flags).join('')

let commands = {
  'clear': clear,
  'help': (...a) => help(commands, ...a),
  'man': man,
  'adduser': adduser,
  'deluser': deluser,
  'moduser': moduser,
  'listuser': listuser,
  'additem': additem,
  'delitem': delitem,
  'moditem': moditem,
  'listitem': listitem,
  'addevent': addevent,
  'delevent': delevent,
  'modevent': modevent,
  'listevent': listevent
}

let parser = (command, env) => {
  let parse = (command) => {
    let tokens = command.split(/ +/)
    let name = tokens.shift()
    let args = {}
    let flags = {}
    let anonArgPos = 0

    while(tokens.length > 0) {
      const token = tokens.shift()
      if(token[0] === '-') {
        if(token[1] === '-') {
          const next = tokens.shift()
          args[token.slice(2)] = next;
        } else {
          token.slice(1).split('').forEach(flag => flags[flag] = true)
        }
      } else {
        args[anonArgPos++] = token
      }
    }
    return { name, flags, command, args }
  }

  let did_run = false

  let run = (l_obj) => {
    Object.keys(commands).forEach(key => {
      if(key === l_obj.name) commands[key](l_obj.command, l_obj.args, l_obj.flags, env)
    })
  }

  if(!did_run) run(parse(command))
  did_run = false
}

export { parser }
