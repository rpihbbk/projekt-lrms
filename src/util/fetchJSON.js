import fetch from './fetch.js'

export default (url, body, method='POST') => fetch(url, body, method).then(res => res.json())
