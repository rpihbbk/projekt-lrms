export default
  `this is the man page for 'clear'.

  'clear' is a command to clear the command prompt.
   use the flag -h for syntax details.`
