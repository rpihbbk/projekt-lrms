export default
  `this is the man page for 'help'.

  'help' is a command to show all available commands in the command prompt.
   use the flag -h for syntax details.`
