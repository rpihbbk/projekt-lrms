export default
  `this is the man page for 'deluser'.

  'deluser' is a command to delete existing users.
   use the flag -h for syntax details.`
