import helppage from './helppage/clear.js'

export default (cmd, args, flags, env) => {
  switch (true) {
    case !!flags.h:
        env.println(helppage)
      break;
    default:
      env.setHistory([])
  }
}
