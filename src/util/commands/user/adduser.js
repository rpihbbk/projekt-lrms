import helppage from '../helppage/user/adduser.js'
import storage from '../../storage.js'

const create_user = (username, fullname, password) =>
  fetch('http://localhost:8080/user/add', {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': storage.getItem('jwt')
    },
    body: JSON.stringify({ username, fullname, password })
  })
  .then(res => res.json())
  .then(json => {
    console.log(json)
/*
.then(res => res.json())
.then(json => {
  console.log(json)
  this.env.println(json)
*/
})

export default (cmd, args, flags, env) => {
  switch (true) {
    case !!flags.h:
        env.println(helppage)
      break;
    case !!args[0] && !!args[1] && !!args[2]:
      create_user.call(this, args[0], args[1], args[2])
      break;
    case !!args[0] && !!args[1] && flags.r:
      let randomnum = Math.random().toString(36).substring(2, 10)
      env.println("Password is: " + randomnum)
      create_user.call(this, args[0], args[1], randomnum)
      break;
    default:
  }
}
