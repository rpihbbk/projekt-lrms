import helppage from './helppage/man.js'
import man from './manpage/man.js'
import help from './manpage/help.js'
import clear from './manpage/clear.js'
import adduser from './manpage/user/adduser.js'
import deluser from './manpage/user/deluser.js'
import moduser from './manpage/user/moduser.js'
import listuser from './manpage/user/listuser.js'
import additem from './manpage/item/additem.js'
import delitem from './manpage/item/delitem.js'
import moditem from './manpage/item/moditem.js'
import listitem from './manpage/item/listitem.js'
import addevent from './manpage/event/addevent.js'
import delevent from './manpage/event/delevent.js'
import modevent from './manpage/event/modevent.js'
import listevent from './manpage/event/listevent.js'

export default (cmd, args, flags, env) => {
  let data = {
    'help': help,
    'clear': clear,
    'man': man,
    'adduser': adduser,
    'deluser': deluser,
    'listuser': listuser,
    'moduser': moduser,
    'additem': additem,
    'delitem': delitem,
    'moditem': moditem,
    'listitem': listitem,
    'addevent': addevent,
    'delevent': delevent,
    'modevent': modevent,
    'listevent': listevent
  }
  switch (true) {
    case !!flags.h:
        env.println(helppage)
      break;
    case !!data[args[0]]:
      env.println(data[args[0]])
      break;
    case !args[0] && !flags[0]:
      env.println('usage: man [man page]')
      break;
    default:
      env.println(`no man page found for '${args[0]}'`)
  }
}
