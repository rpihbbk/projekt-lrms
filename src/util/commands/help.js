import helppage from './helppage/help.js'

export default (commands, cmd, args, flags, env) => {
  switch (true) {
    case !!flags.h:
        env.println(helppage)
      break;
    default:
      env.println('The following commands are available:\n')
      env.println(Object.keys(commands).sort().join('\n'))
  }
}
