
const flatten = (arr) => [].concat(...arr)

const flattenDeep = (arr) => flatten(arr.map(x => Array.isArray(x) ? flattenDeep(x) : x))

const fn = {
  mapObj: (_obj, fn) => {
  let obj = Object.assign({}, _obj)
  Object.keys(obj)
    .map(key => fn(obj[key], key))
    .forEach((new_val, i) => obj[Object.keys(obj)[i]] = new_val)
  return obj
},
  forEachObj: (obj, fn) => Object.keys(obj).map(key => fn(obj[key], key)),
  filter: (arr, fn) => fn ? arr.filter(fn) : arr.filter(x => x ? x : null),
  keys: (obj) => Object.keys(obj),
  values: (obj) => Object.values(obj),
  get: (obj, key) => obj[key],
  getReverse: (obj, key) => Object.keys(obj)[Object.values(obj).indexOf(key)],
  flatten: flatten,
  flattenDeep: flattenDeep,
  pass: (fn) => (...args) => {
    fn(...args)
    return args[0]
  },
  pipe: (obj) => {
    let l_obj = {
      _obj: obj
    }
    Object.values(Object.getOwnPropertyNames(Object.getPrototypeOf(new Date()))).forEach(x =>
      l_obj[x] = (...a) => {
        obj[x](...a)
        return l_obj
      })
    return l_obj
  }
}

export default fn
