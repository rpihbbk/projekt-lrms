let cb_get = []
let cb_set = []
let cb_remove = []
const storage = {
  addEventListener: (type, cb) => {
    if(typeof type !== 'string') {
      return null
    } else if(type === 'get') {
      cb_get.push(cb)
    } else if(type === 'set') {
      cb_set.push(cb)
    } else if(type === 'remove') {
      cb_remove.push(cb)
    }
  },
  getItem: (key) => {
    if(!key) return null
    cb_get.map(cb => cb({
      type: 'get',
      key: key
    }))
    return window.localStorage.getItem(key)
  },
  setItem: (key, value, name, no_listener=false) => {
    if(!key) return null
    if(!value) return
    if(!no_listener) cb_set.map(cb => cb({
      name: name,
      type: 'set',
      key: key,
      newValue: value,
      oldValue: window.localStorage.getItem(key)
    }))
    return window.localStorage.setItem(key, value)
  },
  removeItem: (key) => {
    if(!key) return null
    cb_remove.map(cb => cb({
      type: 'remove',
      key: key,
      oldValue: window.localStorage.getItem(key)
    }))
    return window.localStorage.removeItem(key)
  },
  json: {
    getItem: (key) => {
      const val = storage.getItem(key)
      if(val !== null) {
        return JSON.parse(val)
      } else {
        return null
      }
    },
    setItem: (key, value, name, no_listener) => {
      if(typeof value === 'object') {
        const val = JSON.stringify(value)
        storage.setItem(key, val, name, no_listener)
      } else {
        return null
      }

    },
    removeItem: (key) => {
      return storage.removeItem(key)
    }
  }
}

export default storage
