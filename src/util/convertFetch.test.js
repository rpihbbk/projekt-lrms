import { convertDay, convertWeek } from './convertFetch.js'

const server_event = {
  teacher: 'AfdTuWaT8TToGKJb',
  item: 1,
  start: 1,
  end: 2,
  date: '2017-10-27T00:00:00.000Z',
  room: 307,
  title: 'test numero unendlich+1 vermutlich inzwischen',
  recurring: false,
  recurring_time: 0,
  _id: 'mVovXbkIRmxGBJfV'
}

const client_event = {
  reserved: true,
  person: 'AfdTuWaT8TToGKJb',
  title: 'test numero unendlich+1 vermutlich inzwischen',
  item: 1,
  room: 307,
  id: 'mVovXbkIRmxGBJfV',
  start: 1,
  end: 2,
  date: '2017-10-27'
}

it('convertFetch convertDay', () => {
  let hours = [...Array(12).keys()].map(j => ({
    reserved: false,
    person: '',
    title: '',
    room: undefined,
    item: undefined
  }))

  hours[0] = client_event
  hours[1] = client_event

  expect(convertDay([server_event])).toEqual(hours)
})

it('convertFetch convertWeek', () => {
  let hours = [...Array(12).keys()].map(j => [...Array(7).keys()].map(j => ({
    reserved: false,
    person: undefined,
    room: undefined,
    title: undefined,
    item: undefined,
  })))

  hours[0][4] = client_event
  hours[1][4] = client_event

  expect(convertWeek([server_event])).toEqual(hours)
})
