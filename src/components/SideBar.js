import React from 'react'
import pt from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import '../css/SideBar.css'

const SideBar = styled.nav`
  position: relative;
  float: left;
  width: 200px;
  height: calc(100vh - 64px);
  text-align: left;
  background-color: ${props => props.theme.SideBar.bg1};
`

const ItemWrapper = styled.div`
  > .item.title {
    height: 48px;
    background-color: ${props => props.theme.SideBar.bg4};
    /* background-color: #3c565d!important;
    background-color: hsla(338, 78%, 38%, 1) */
  }
  > .item {
    position: relative;
    width: 100%;
    box-sizing: border-box;
    line-height: 48px;
    padding-left: 12px;
    font-size: 16px;
    display: block;
    color: ${props => props.theme.SideBar.color}; /* color1 */
    background-color: ${props => props.active === true ? `${props.theme.SideBar.bg2}!important` : `${props.theme.SideBar.bg3};`} /* bg2 #37474F / bg3 #47656d*/
    text-decoration: none;
  }

  > .item:focus, .item:hover {
    filter: brightness(80%);
  }

  > .item.no_hover:focus, .item.no_hover:hover {
    filter: none;
  }

  > .item.active {
    background-color: ${props => props.theme.SideBar.bg5}!important;
  }

  > .item.active:focus, .item.active:hover {
    filter: brightness(80%);
  }

  > .item .material-icons {
    position: relative;
    display: inline-block;
    top: 6px;
    margin-right: 8px;
  }
`

const MaterialIcon = ({icon}) => (
  <i className="material-icons">{icon}</i>
)

const IconSpan = ({icon, children}) => (
  <span>
    <MaterialIcon icon={icon} />
    <span style={{paddingLeft: icon ? '0px' : '24px'}}>{children}</span>
  </span>
)

const ItemComponent = ({view, active}) => (
  view.url
  ? <Link className={`item ${view.className}${active ? ' active' : ''}`} to={view.url}>
      <IconSpan icon={view.icon}>{view.name}</IconSpan>
    </Link>
  : <span className={`item ${view.className}${active ? ' active' : ''}`}>
      <IconSpan icon={view.icon}>{view.name}</IconSpan>
    </span>
)

const _SideBar = ({ views, active }) => (
  <SideBar className="SideBar" role="navigation">
    <ItemWrapper>
      {views.map((view, i) => 
        <ItemComponent view={view} key={i} active={active === view.id} />
      )}
    </ItemWrapper>
  </SideBar>
)

_SideBar.propTypes = {
  views: pt.arrayOf(pt.shape({
    name:       pt.string,
    _name:      pt.string,
    icon:       pt.string,
    className:  pt.string,
    id:         pt.number
  })),
  active: pt.number
}

export default _SideBar
