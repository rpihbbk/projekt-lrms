import styled from 'styled-components'

const Title = styled.h3`
  padding: 24px 5% 8px 5%;
  margin-bottom: 8px;
  text-align: left;
`

const TitleSmall = styled.h5`
  padding: 4px 16px 4px 5%;
  margin-bottom: 8px;
`

const Ul = styled.ul`
  list-style: circle inside;
  margin-top: 0px;
`

const Li = styled.li`
  font-weight: 300;
  font-size: 14px;
  line-height: 19px; /* am liebsten wäre das mir mit na geraden Zahl -.-*/
`

const Span = styled.span`
  display: inline-block;
  padding-left: 7%;
  font-size: 14px;
  margin-top: 8px;
  font-weight: 300;
`

const Hr = styled.hr`
  width: 95%;
  position: relative;
  left: 2.5%;
  margin-top: 16px;
  margin-bottom: 16px;
  border-width: 0;
  border-top: 1px solid #dedede;
  clear: both; /* fuck you firefox, why do you not work properly without this like the other browsers (EVEN EDGE!!!)*/
`

const Select = styled.select`
  height: 28px;
  min-width: 64px;
  text-align: center;
  font-family: arial;
  font-size: 11px;
  padding: 6px 10px; /* The 6px vertically centers text on FF, ignored by Webkit */
  border-radius: 4px;
  box-shadow: none;
  box-sizing: border-box;
  background-color: ${props => props.theme.Elements.bg1};
  color: ${props => props.theme.Elements.color};
  border: 1px solid ${props => props.theme.Elements.border1};

  :focus {
    border: 1px solid ${props => props.theme.Elements.color2};
    background-color: ${props => props.theme.Elements.bg2};
    outline: 0;
  }
`

export {
  Title, TitleSmall, Ul, Li, Span, Hr, Select
}
