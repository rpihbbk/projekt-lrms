import React from 'react'
import styled from 'styled-components'
import pt from 'prop-types'

import aria from '../util/aria-support-helpers.js'

const PopupWrapper = styled.div`
  display: ${props => props.hidden ? 'none' : 'block'}
`

const Overlay = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  backface-visibility: hidden;
`

/*
transition: opacity 500ms;
visibility: visible;
opacity: 1;
*/

const Popup = styled.div`
  margin: 6% auto 0 auto;
  padding: 4px;
  background: #fff;
  border-radius: 5px;
  width: 40%;
  position: relative;
`
/* transition: all 2s ease-in-out; */

const Title = styled.div`
  position: relative;
  float: left;
  text-align: left;
  top: 6px;
  left: 10px;
  font-size: 18px;
`

const Hr = styled.hr`
  position: relative;
  top: 10px;
  left: -4px;
  width: calc(100% + 8px);
  margin-bottom: 16px;
  border-width: 0;
  border-top: 1px solid #dedede;
  clear: both; /* fuck you firefox, why do you not work properly without this like the other browsers (EVEN EDGE!!!)*/
`

const Close = styled.div`
  position: relative;
  float: right;
  top: 6px;
  right: 6px;

  color: #000000;

  > i:hover {
    color:#FF0000;
  }
`
/* transition: all 200ms; */

const Content = styled.div`
  margin: 8px 0px;
`

const Header = styled.div`
  position: relative;
  height: 44px;
`

const _Popup = ({ hidden, children, title = "", close }) => (
  <PopupWrapper onKeyPress={e => aria.esc(e, close)} hidden={hidden} tabIndex={-10} role="dialog">
    <Overlay>
      <Popup>
        <Header>
          <Title>{title}</Title>
          <Close tabIndex={0} onKeyPress={e => aria.spacebar(e, () => close(false))} onClick={() => close(false)}><i className="material-icons">close</i></Close>
          <Hr />
        </Header>
        <Content>{children}</Content>
      </Popup>
    </Overlay>
  </PopupWrapper>
)

_Popup.propTypes = {
  hidden: pt.bool,
  title:  pt.string,
  close:  pt.func
}

export default _Popup
