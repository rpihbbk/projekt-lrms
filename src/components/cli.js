import React from 'react'
import styled from 'styled-components'

import Terminal from './Terminal.js'

const CLI = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`

export default props => (
  <CLI>
    <Terminal headless={true} />
  </CLI>
)
