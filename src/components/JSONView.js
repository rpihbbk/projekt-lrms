import React from 'react'
import pt from 'prop-types'

import JSON from './json.js'

const JsonView = ({ json }) => (
  <div style={{margin: '8px'}}>
    <JSON json={{json}} />
  </div>
)

JsonView.propTypes = {
  json: pt.oneOfType([pt.object, pt.array])
}

export default JsonView
