import React, { Component } from 'react'
import styled from 'styled-components'
import pt from 'prop-types'
import aria from '../util/aria-support-helpers.js'

const Bar = styled.div`
  cursor: pointer;
  position: relative;
  width: 100%;
  font-size: 13px;

  :hover, :focus {
    background-color: hsl(193, 88%, 91%);
  }
`

const Content = styled.div`
  display: ${props => props.hidden ? 'none' : 'block'};
  font-size: 13px;
  border-left: 3px solid hsla(193, 21%, 75%, 1);
  margin-left: 5px;
  margin-top: 4px;
  padding-left: 8px;
`

class Spoiler extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: this.props.open,
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      open: nextProps.open
    })
  }

  render() {
    return (
      <div>
        <Bar
          tabIndex={0}
          onKeyPress={e => aria.spacebar(e, () => this.setState({ open: !this.state.open}))}
          onClick={() => this.setState({ open: !this.state.open})}>
          {this.state.open === true ? '▲' : '▼'} {this.props.title}
        </Bar>
        <Content {...(!this.state.open ? {hidden: true} : {})}>
          {this.props.children}
        </Content>
      </div>
    )
  }
}

Spoiler.propTypes = {
  open: pt.bool,
  title: pt.string
}

export default Spoiler
