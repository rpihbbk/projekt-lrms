import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'

import fetchJSON from '../util/fetchJSON'

import styled from 'styled-components'
import '../css/App.css'

import SideBar from './SideBar.js'
import MonthView from './MonthView.js'
import WeekView from './WeekView.js'
import DayView from './DayView.js'

import Settings from './Settings.js'
import Information from './Information.js'
import CLI from './cli.js'
import DB from './db.js'

import JSONView from './JSONView.js'
import { dependencies } from '../../package.json'

const AppWrapper = styled.div`
  text-align: center;
`

const MainWrapper = styled.main`
position: fixed;
box-sizing: border-box;
/*padding: 0px 16px;*/
padding: 0px 0px;
float: left;
width: calc(100vw - 200px);
height: calc(100vh - 64px);
background-color: ${props => props.theme.main.bg};
color: ${props => props.theme.main.color};
overflow-y: auto;
left: 200px;
`

const Error = styled.div`
position: absolute;
width: 200px;
background-color: white;
padding: 10px;
margin-left: calc(50% - 100px);
box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
`

const views = [
  {name: 'Ansichten', _name: 'views', icon: 'today', className: 'title no_hover', id: 0},
  {name: 'Monat', url: '/interface/month', _name: 'month', icon: '', className: '', id: 1},
  {name: 'Woche', url: '/interface/week', _name: 'week', icon: '', className: '', id: 2},
  {name: 'Tag', url: '/interface/day', _name: 'day', icon: '', className: '', id: 3},
  {name: 'Einstellungen', url: '/interface/settings', _name: 'settings', icon: 'settings', className: 'title', id: 4},
  {name: 'Informationen', url: '/interface/information', _name: 'information', icon: 'info_outline', className: 'title', id: 5},
  {name: 'Admin Dashboard', _name: 'admin', icon: 'code', className: 'title no_hover', id: 6},
  {name: 'Konsole', url: '/interface/cli', _name: 'cli', icon: '', className: '', id: 7},
  {name: 'Datenbank', url: '/interface/db', _name: 'db', icon: '', className: '', id: 8},
]

export default class App extends Component {
  constructor(props) {
    super(props)
    console.log(this.props)
    this.getActive = this.getActive.bind(this)
    this.view = this.getActive()
    this.state = {
      status: null,
      status_err: null
    }
    this.getStatus.bind(this)()
  }

  getStatus() {
    fetchJSON(window.BACKEND + '/status/', null, 'GET')
      .then(json => {
        console.log('server is up', json)
        this.setState({ status: json })
      })
      .catch(err => {
        console.log('server unreachable', err)
        this.setState({ status_err: err }, x => console.log(this.state, x))
      })
  }

  getActive() {
    return views
      .map(view =>
        view._name === (this.props.location.pathname.match(/^\/interface\/([\w|\d|.|@|:|!||~|$|%]+)\/?/i) || [null, 'month'])[1] ? view.id : -1) // setting 'month' to be the active view when no other view is indicated to be active via the url
      .filter(x => x !== -1)[0]
  }

  render() {
    return (
      <AppWrapper className={`App`}>
        <SideBar views={views} active={this.getActive()} />
        <MainWrapper className="Main" role="main">
          <Switch>
            <Route exact path="/interface" component={MonthView} />
            <Route exact path="/interface/settings" component={Settings} />
            <Route exact path="/interface/information/packages" render={({ match }) => (
              <JSONView json={dependencies} />
            )} />
            <Route exact path="/interface/information/" component={Information} />
            <Route exact path="/interface/cli" component={CLI} />
            <Route exact path="/interface/db" component={DB} />
            <Route exact path="/interface/month/:date?/" component={MonthView} />
            <Route exact path="/interface/week/:date?/" component={WeekView} />
            <Route exact path="/interface/day/:date?/" component={DayView} />
            <Route render={({ match }) => (
              <span>
                {"Something went wrong. "}<br />
                {`no matching routes: (${match.url})`}
              </span>
            )} />
          </Switch>
        </MainWrapper>
        {this.state.status_err ? <Error><span><b>Error</b><br />The server seems to be offline. Try <b>reloading</b> as soon as its back up.</span></Error> : null}
      </AppWrapper>
    )
  }
}
