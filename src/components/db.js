import React, { Component } from 'react'

import styled from 'styled-components'

const DBWrapper = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  padding: 8px;
  color: ${props => props.theme.db.color};
`

const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
`

const Cell = styled.td`
  font-size: 12px;
  cursor: cell;
  padding: 0px 2px;
  text-align: right;
  height: 18px;
  background-color: ${props => props.selected ? props.theme.db.bg2 : props.theme.db.bg3};
  border: 1px ${props => props.selected ? `double ${props.theme.db.border1}` : `solid ${props.theme.db.border2}`};
  box-shadow: ${props => props.selected ? 'inset 0 -100px 0 rgba(33, 133, 208, 0.15)' : 'none'};
`

const Name = styled.td`
  border: 1px ${props => props.selected ? `double ${props.theme.db.border3}` : `solid ${props.theme.db.border4}`};
  background-color: ${props => props.theme.db.bg1};
  font-size: 12px;
  cursor: cell;
  padding: 0px;
  text-align: center;
  height: 18px;
`

const isFocused = (focused, x, y) => focused.x === x && focused.y === y

const cell = (c, j, k, focused, cb) => (
  <Cell onClick={cb.bind(this, j, k+1)}
      selected={isFocused(focused, j, k+1)}
      id={(k+1) + '.' + j}
      key={(k+1) + '.' + j}>
    {c}
  </Cell>
)

const name = (c, j, k=0, focused, cb) => (
  <Name onClick={cb.bind(this, j, k)}
      selected={isFocused(focused, j, k)}
      id={k + '.' + j}
      key={k + '.' + j}>
    {c}
  </Name>
)

const coordinates_to_id = (x, y) => ('∆ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')[x]) + (y ? y : '∆')

export default class DB extends Component {
  constructor(props) {
    super(props)

    this.focused = {x: 1, y: 1}
    this.cb = (x, y, ...args) => {
      console.log(coordinates_to_id(x, y))
      this.focused = {x, y}
      this.forceUpdate()
    }
    this.name = (c, j, k=0) => name(c, j, k, this.focused, this.cb)
    this.cell = (c, j, k) => cell(c, j, k, this.focused, this.cb)

    this.rows = [
      'user_id #', 'user_name aA-zZ', 'created_at date', 'idfk'
    ]
    this.data = {
      '1': [
        {key: 'A1', value: '00000001'},
        {key: 'B1', value: 'jannik 0'},
        {key: 'C1', value: '11.2.17'},
        {key: 'D1', value: 'D1'},
      ],
      '2': [
        {key: 'A2', value: '00000002'},
        {key: 'B2', value: 'jannik 1'},
        {key: 'C2', value: '12.3.17'},
        {key: 'D2', value: 'D2'},
      ],
      '3': [
        {key: 'A3', value: '00000003'},
        {key: 'B3', value: 'jannik 2'},
        {key: 'C3', value: '13.4.17'},
        {key: 'D3', value: 'D3'},
      ],
      '4': [
        {key: 'A4', value: '00000004'},
        {key: 'B4', value: 'jannik 3'},
        {key: 'C4', value: '14.5.17'},
        {key: 'D4', value: 'D4'},
      ]
    }
  }

  sort(filter, direction) {
    console.log(filter, direction)
  }


  render() {
    return (
      <DBWrapper>
        <Table>
          <tbody>
          <tr id={0}>
            {this.name('', 0, 0)}
            {this.rows.map((key, i) => this.name(key, i+1, 0))}
          </tr>
          {Object.keys(this.data).map((key, j) => (
            <tr id={j+1} key={j+1}>
              {this.name(j, 0, j+1)}
              {this.data[key].map((field, k) => this.cell(field.value, k+1, j))}
            </tr>
          ))}
          </tbody>
        </Table>
      </DBWrapper>
    )
  }
}
