import React, { Component } from 'react'

import dates from '../util/dates.js'
import fetchJSON from '../util/fetchJSON.js'
import { convertWeek } from '../util/convertFetch.js'
import aria from '../util/aria-support-helpers.js'
import { namelookup } from '../util/lookup.js'

import PopupWrapper from './PopupWrapper'
import fn from '../util/fn.js'

import NavButton from './NavButton.js'
import FAB from './FAB.js'
import WeekViewItem from './WeekViewItem.js'
import { Select } from './Elements.js'

import '../css/WeekView.css'

import { WeekViewWrapper, TitleWrapper, TitleMonth, TitleYear, TitleWeek, Table, InputContainer, BtnContainer } from './WeekViewStyled.js'

const flattenDeep = fn.flattenDeep
const pass = fn.pass

const generateDateVariables = (date) => {
  const l_date = date
  let l_vars = { date: l_date, year: l_date.getFullYear(), month: l_date.getMonth() }
  l_vars._startOfWeek = l_date.getDate() - (l_date.getDay() === 0 ? 6 : l_date.getDay() - 1)
  l_vars._endOfWeek = l_vars._startOfWeek + 6
  l_vars.startOfWeek = new Date(l_date.getFullYear(), l_date.getMonth(), l_vars._startOfWeek)
  l_vars.endOfWeek = new Date(l_date.getFullYear(), l_date.getMonth(), l_vars._endOfWeek)
  l_vars.year = l_date.getFullYear()
  l_vars.month = l_date.getMonth()
  l_vars.days = [0, 1, 2, 3, 4, 5, 6].map(i =>
    new Date(l_vars.startOfWeek.getFullYear(), l_vars.startOfWeek.getMonth(), l_vars.startOfWeek.getDate() + i).getDate()
  )
  return l_vars
}

// route changing
const changeRouteTo = (route, history, bool=true) => {
  if(bool) history.push(route)
}

export default class WeekView extends Component {
  constructor(props) {
    super(props)

    this.state = this.generateState(dates.parseDateInfo(this.props.match.params.date))
  }

  generateEmptyHourState() {
    return [...Array(12).keys()].map(j => [...Array(7).keys()].map(j => ({
      reserved: false,
      person: undefined,
      room: undefined,
      title: undefined,
      item: undefined,
    })))
  }

  generateState(date) {
    const date_var = generateDateVariables(date)
    return {
      date: date_var,
      hours: this.generateEmptyHourState(),
      popup: '',
      open: false,
      data: dates.stringifyDateInfoPadded(date_var.date),
      close: () => this.setState({ open: false })
    }
    // state:
    //   data: idk
    //   hours[]
  }

  fetch(date) {
    return fetchJSON(window.BACKEND + '/event/get', {
      date: {
        end:    dates.setTimeIgnoreTimezone(this.state.date.endOfWeek, 23, 59, 59, 999).toISOString(),  // end first because i want date to
        start:  dates.setTimeIgnoreTimezone(this.state.date.startOfWeek, 0, 0, 0, 0).toISOString()        // be the start of the day in state
      }
    }, 'POST')
      .then(res => convertWeek(res))
      .then(pass(res => Array.from(new Set(
          flattenDeep(res)
            .filter(x => x.reserved)
            .map(x => x.person)
        ))
          .forEach(x => namelookup(x))
      ))
      .then(res => this.setState({hours: res}))
  }

  componentWillReceiveProps(nextProps) {
    const date = dates.parseDateInfo(nextProps.match.params.date)
    this.setState(this.generateState(date), () => this.fetch(date))
  }

  componentWillMount() {
    this.fetch(dates.parseDateInfo(this.props.match.params.date))
  }

  render() {
    return (
      <WeekViewWrapper className="week-view">
        <PopupWrapper open={this.state.open} popup={this.state.popup} data={this.state.data} close={this.state.close}/>
        <TitleWrapper>
          <TitleMonth>{dates.months[this.state.date.month]}</TitleMonth>
          <TitleYear>{this.state.date.year}</TitleYear>
          <TitleWeek>{this.state.date.startOfWeek.getDate() + ' - ' + this.state.date.endOfWeek.getDate()}</TitleWeek>
        </TitleWrapper>
        <Table id="weektable">
          <tbody>
            <tr>
            <th>Std</th>
              {this.state.date.days.map((day, i) => (
                <th tabIndex={0} onClick={() => changeRouteTo(`/interface/day/${dates.stringifyDateInfo(this.state.date.startOfWeek, 0, i)}`, this.props.history)} key={i}>
                  {dates.week_days_short[i] + " " + day}
                </th>
              ))}
            </tr>
            {this.state.hours.map((hour, j) => (
              <tr key={j}>
                <td key={j + '.' + 0} className="no_border">{j+1}</td>
                {hour.map((event, k) =>
                  event.reserved
                    ? <WeekViewItem key={j + "." + k+1} event={event} onUse={() => this.setState({popup: 'event', open: true, data: event})} />
                    : <td key={j + "." + k+1} />
                )}
              </tr>
            ))}
          </tbody>
        </Table>
        <BtnContainer>
          <NavButton changeRoute={changeRouteTo.bind(this,
            `/interface/week/${dates.stringifyDateInfo(this.state.date.date, 0, -7)}`, this.props.history)}>
            arrow_back
          </NavButton>
          <NavButton changeRoute={changeRouteTo.bind(this,
            `/interface/week/${dates.stringifyDateInfo(this.state.date.date, 0, +7)}`, this.props.history)}>
            arrow_forward
          </NavButton>
          <FAB role="button" tabIndex={0} className="btn"
               onKeyPress={e => aria.spacebar(e, () => this.setState({popup: 'add', open: true}))}
               onClick={() => this.setState({popup: 'add', open: true})}>
            <i className="material-icons">add</i>
          </FAB>
          <InputContainer>
            <Select aria-label="Laptop-wagen Auswahl (nach Raum)" innerRef={input => this.room_el = input} style={{margin: '4px'}} defaultValue="R111">
              <option value="R111">R111</option>
              <option value="R222">R222</option>
              <option value="R333">R333</option>
            </Select>
          </InputContainer>
        </BtnContainer>
      </WeekViewWrapper>
    )
  }
}
