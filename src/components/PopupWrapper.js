import React, { Component } from 'react'

import fetch from '../util/fetch.js'
import { idlookup } from '../util/lookup.js'

import Popup from './Popup.js'
import AddPopup from './AddPopup.js'
import InfoPopup from './InfoPopup.js'


const _diff = (x, y) => Object.keys(x).map((z) => x[z] === y[z] || +new Date(x[z]) === +new Date(y[z]) ? null : z).filter(z => z !== null)
const diff = (x, y) => {
  let obj = {}
  _diff(x, y).forEach(z => obj[z] = x[z])
  console.log(x, y, obj, _diff(x, y))
  return obj
}

const postAdd = (data, close) => {

  const send = (data) => (console.log(data), fetch(window.BACKEND + '/event/add', data, 'POST').then(x => close(true)))

  const transformAdd = (data, personId) =>
    Object.assign(data, {
      person: personId ? personId : data.person,
      end: data.end ? data.end : data.start
    })

  if(data.person.startsWith('id:')) {
    data.person = data.person.substring(3)
    send(transformAdd(data))
  } else {
    const lookup = idlookup(data.person)
    if(typeof lookup === 'string') {
      data.person = lookup
      send(transformAdd(data))
    }
    else {
      lookup.then(id => send(transformAdd(data, id)))
    }
  }
}

const postMod = (modifiedData, originalData, close) => {

  const send = (data) => fetch (window.BACKEND + '/event/mod', data, 'POST').then(x => close(true))

  const new_obj = Object.assign(diff(modifiedData, originalData), {id: originalData.id})

  if(new_obj.person) {
    if(new_obj.person.startsWith('id:')) {
      new_obj.person = new_obj.person.substring(3)
      console.log(modifiedData, originalData, new_obj)
      send(new_obj)
    } else {
      const lookup = idlookup(new_obj.person)
      if(typeof lookup === 'string') {
        new_obj.person = lookup
        console.log(modifiedData, originalData, new_obj)
        send(new_obj)
      } else {
        lookup.then(id => {
          console.log(modifiedData, originalData, new_obj)
          send(Object.assign(new_obj, {person: id}))
        })
      }
    }
  } else {
    send(new_obj)
  }
}

const postDel = (data, close) => {
  const send = (data) => fetch(window.BACKEND + '/event/del', data, 'POST').then(x => close(true))

  send(data)
}

const popups = {
  add: (close, data) => ({
    title: 'Event hinzufügen',
    component: (
      <AddPopup
        defaultData={data}
        cb={(data) => { console.log('add', data);postAdd(data, close)}}
        close={close}
      />
    )
  }),
  modify: (close, data) => ({
    title: 'Event editieren',
    component: (
      <AddPopup
        defaultData={data}
        cb={(modifiedData) => { console.log('modify', data);postMod(modifiedData, data, close) }}
        del={() => { console.log('delete', data);postDel({id: data.id}, close) }}
        close={close}
        key={data.id}
      />
    )
  }),
  event: (close, event) => ({
    title: 'Event',
    component: (
      <InfoPopup close={close} event={event} key={event.id} />
    )
  }),
}


export default class PopupWrapper extends Component {
  constructor(props) {
    super(props)
    this.state = {
      popup: this.props.popup,
      close: this.props.close,
      data: this.props.data
    }
  }

  popup() {
    if(popups[this.state.popup]) {
      return popups[this.state.popup](this.state.close, this.state.data)
    } else {
      return {title: '', comoponent: ''}
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      popup: nextProps.popup,
      close: nextProps.close,
      data: nextProps.data
    })
  }

  render() {
    let _popup = this.popup()
    return (
      <Popup title={_popup.title} hidden={!this.props.open}
             close={this.state.close}>
        {_popup.component}
      </Popup>
    )
  }
}
