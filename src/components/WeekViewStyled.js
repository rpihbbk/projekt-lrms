import styled from 'styled-components'

const WeekViewWrapper = styled.div`
  padding: 8px;
  float: left;
`

const TitleWrapper = styled.div`
  text-align: left;
  margin-left: 12px;
`

const TitleMonth = styled.span`
  font-size: 26px;
  font-weight: 600;
`

const TitleYear = styled.span`
  margin: 0px 12px 0px 6px;
  font-size: 26px;
`

const TitleWeek = styled.span`
  font-size: 16px;
`

const Table = styled.table`
  display: inline-block;
  float: left;
  border-spacing: 0;

  > tbody tr th {
    width: 48px;
    height: 48px !important;
    border-bottom: 2px solid ${props => props.theme.WeekView.border};
    cursor: pointer;
  }

  > tbody tr th:hover, tbody tr th:focus {
    color: hsl(193, 88%, 26%);
  }

  > tbody tr td {
    width: 48px;
    height: 40px !important;
    border: 0px;
    border-right: 1px solid ${props => props.theme.WeekView.border};
    border-bottom: 1px solid ${props => props.theme.WeekView.border};
    border-spacing: 0;
  }

  > tbody tr td.no_border {
    border: none;
  }

  > tbody tr td.no_border:hover {
    background-color: inherit;
    cursor: inherit;
  }

  > tbody td span.week-hours {
    line-height: 48px;
  }

  > tbody tr td:hover {
    background-color: ${props => props.theme.WeekView.bg_hover};
    cursor: pointer;
  }
`

const InputContainer = styled.div`
  display: inline-block;
  margin-top: 12px;
`

const BtnContainer = styled.div`
  display: inline-block;
  float: left;
  width: 64px;
  height: 128px;
  margin-top: 48px;
`

export { WeekViewWrapper, TitleWrapper, TitleMonth, TitleYear, TitleWeek, Table, InputContainer, BtnContainer }
