import React, { Component } from 'react'

import dates from '../util/dates.js'

import storage from '../util/storage.js'

import fetchJSON from '../util/fetchJSON.js'

import aria from '../util/aria-support-helpers.js'

import convert from '../util/convertFetch.js'

import pipe from '../util/pipe.js'

//import flow from '../util/flow-css/index.js' /* using flow right now is not really practical, since it does not support tagged template literals (passing them through to styled-components AND compiling them would be good but really difficult I presume, also supporting selectors would be really crucial for this to work well) */

import NavButton from './NavButton.js'
import FAB from './FAB.js'
import DayViewItem from './DayViewItem.js'

import PopupWrapper from './PopupWrapper.js'

import { Select } from './Elements.js'

import { DayViewWrapper, TitleWrapper, TitleMonth, TitleYear, TitleWeek, InputContainer, BtnContainer, Table } from './DayViewStyled.js'

const generateDateVariables = (date) => {
  const l_date = date
  let l_vars = {
    date: l_date,
    year: date.getFullYear(),
    month: date.getMonth()
  }
  return l_vars
}

const changeRouteTo = (route, history, bool=true) => {
  if(bool) history.push(route)
}

export default class DayView extends Component {
  constructor(props) {
    super(props)
    this.room_el = undefined

    this.state = Object.assign({
      popup: '',  // not really important data in case of URL change, can be reset
      open: false,
      data: this.getDefaultData(),
      close: () => this.setState({ open: false }),
      hours: [...Array(12).keys()].map(j => ({
        reserved: false,
        person: undefined,
        room: undefined,
        title: undefined,
        item: undefined,
      }))
    }, generateDateVariables(dates.parseDateInfo(this.props.match.params.date)))
    /* eslint ignore-next */ // this does not count as 'fixing' something, will be done later
    this.state.hours = this.fetchData()
  }

  updateDateState(dateStr) {
    this.setState(generateDateVariables(dates.parseDateInfo(dateStr || this.props.match.params.date)))
  }

  updateEventState() {
    this.setState({
      hours: this.fetchData()
    })
  }

  updateDefaultData(dateStr) {
    this.setState({data: this.getDefaultData(dateStr)})
  }

  getDefaultData(dateStr) {
    return {
      date: dates.stringifyDateInfoPadded(dates.parseDateInfo(dateStr || this.props.match.params.date))
    }
  }

  fetchData() { // will fetch from server, so not really state, but needs to be cached
    fetchJSON('http://localhost:8080/event/get', {
      date: {
        end: pipe(this.state.date).setHours(23,59,59,999)._obj.toISOString(),  // end first because I want date to
        start: pipe(this.state.date).setHours(0,0,0,0)._obj.toISOString()      // be the start of the day in state
      }
    }, 'POST')
      .then(res => console.log(convert(res)))

    let hours = [...Array(12).keys()].map(j => ({
      reserved: false,
      person: '',
      room: undefined,
      title: '',
      item: undefined
    }))

    hours[1] = {
      reserved: true,
      person: 'BRA',
      room: 123,
      title: '',
      item: 1,
    }

    hours[2] = {
      reserved: true,
      person: 'jannikwibker@gmail.com',
      room: 123,
      title: '',
      item: 1,
    }

    hours[4] = {
      reserved: true,
      person: 'root',
      room: 307,
      title: '',
      item: 1,
    }

    return hours
  }

  componentWillReceiveProps(nextProps) {
    this.updateDateState(nextProps.match.params.date)
    this.updateDefaultData(nextProps.match.params.date)
  }

  render() {
    return (
      <DayViewWrapper className="day-view">
        <PopupWrapper open={this.state.open} popup={this.state.popup} data={this.state.data} close={this.state.close}/>
        <TitleWrapper>
          <TitleMonth>{dates.months[this.state.month]}</TitleMonth>
          <TitleYear>{this.state.year}</TitleYear>
          <TitleWeek>{this.state.date.getDate()}</TitleWeek>
        </TitleWrapper>
        <Table id="daytable">
          <tbody>
            <tr>
            <th>Std</th>
              <th className="timetable">{dates.week_days_short[this.state.date.getDay() === 0 ? 6 : this.state.date.getDay() - 1] + " " + this.state.date.getDate()}</th>
            </tr>
            {this.state.hours.map((hour, j) => (
              <tr key={j}>
                <td className="no_border">{j+1}</td>
                {hour.reserved
                  ? <DayViewItem
                      event={hour}
                      canEdit={hour.person === storage.getItem('user') || storage.getItem('user') === 'root'}
                      onUse={() => {console.log('show');this.setState({popup: 'event', open: true, data: hour})}}
                      onEdit={() => {console.log('edit');this.setState({popup: 'modify', open: true, data: hour})}}
                    />
                  : <td className={'timetable'}></td>
                }
              </tr>
            ))}
          </tbody>
        </Table>
        <BtnContainer>
          <NavButton changeRoute={changeRouteTo.bind(this,
            `/interface/day/${dates.stringifyDateInfo(this.state.date, 0, -1)}`, this.props.history)}>
            arrow_back
          </NavButton>
          <NavButton changeRoute={changeRouteTo.bind(this,
            `/interface/day/${dates.stringifyDateInfo(this.state.date, 0, +1)}`, this.props.history)}>
            arrow_forward
          </NavButton>
          <FAB role="button" tabIndex={0} className="btn"
               onKeyPress={e => aria.spacebar(e, () => this.setState({popup: 'add', open: true}))}
               onClick={() => this.setState({popup: 'add', open: true})}>
            <i className="material-icons">add</i>
          </FAB>
          <InputContainer>
            <Select aria-label="Laptop-wagen Auswahl (nach Raum)" innerRef={input => this.room_el = input} style={{margin: '4px'}} defaultValue="R111">
              <option value="R111">R111</option>
              <option value="R222">R222</option>
              <option value="R333">R333</option>
            </Select>
          </InputContainer>
        </BtnContainer>
      </DayViewWrapper>
    )
  }
}
