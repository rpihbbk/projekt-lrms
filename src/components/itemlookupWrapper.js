import React, { Component } from 'react'
import pt from 'prop-types'

import { itemlookup } from '../util/lookup'

class ItemlookupWrapper extends Component {
  constructor(props) {
    super(props)
    const lookup = itemlookup(props.id)

    console.log(lookup)

    this.state = {
      id: props.id,
      item: typeof(lookup.name) === 'string' ? lookup : null,
      fn: typeof(lookup.then) === 'function' ? lookup : null,
     }
  }

  componentDidMount() {
    if(!this.state.item)
      this.state.fn.then(item => this.setState({item: item, fn: null}))
  }

  render() {
    return (
      <span>{this.state.item ? this.state.item.name : (this.props.idFallback ? this.state.id : this.props.fallbackString) || '...'}</span>
    )
  }
}

ItemlookupWrapper.propTypes = {
  id:             pt.number,
  idFallback:     pt.bool,
  fallbackString: pt.string
}

export default ItemlookupWrapper
