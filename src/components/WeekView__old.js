import React, { Component } from 'react'

//import styled from 'styled-components'

import dates from '../util/dates.js'

import aria from '../util/aria-support-helpers.js'

//import flow from '../util/flow-css/index.js' /* using flow right now is not really practical, since it does not support tagged template literals (passing them through to styled-components AND compiling them would be good but really difficult I presume, also supporting selectors would be really crucial for this to work well) */

import NavButton from './NavButton.js'
import FAB from './FAB.js'
import WeekViewItem from './WeekViewItem.js'

import PopupWrapper from './PopupWrapper'

import { Select } from './Elements.js'

import '../css/WeekView.css'

import { WeekViewWrapper, TitleWrapper, TitleMonth, TitleYear, TitleWeek, Table, InputContainer, BtnContainer } from './WeekViewStyled.js'

// const WeekViewWrapper = styled.div`
//   padding: 8px;
//   float: left;
// `
//
// const TitleWrapper = styled.div`
//   text-align: left;
//   margin-left: 12px;
// `
//
// const TitleMonth = styled.span`
//   font-size: 26px;
//   font-weight: 600;
// `
//
// const TitleYear = styled.span`
//   margin: 0px 12px 0px 6px;
//   font-size: 26px;
// `
//
// const TitleWeek = styled.span`
//   font-size: 16px;
// `
//
// const Table = styled.table`
//   display: inline-block;
//   float: left;
//   border-spacing: 0;
//
//   > tbody tr th {
//     width: 48px;
//     height: 48px !important;
//     border-bottom: 2px solid ${props => props.theme.WeekView.border};
//     cursor: pointer;
//   }
//
//   > tbody tr th:hover, tbody tr th:focus {
//     color: hsl(193, 88%, 26%);
//   }
//
//   > tbody tr td {
//     width: 48px;
//     height: 40px !important;
//     border: 0px;
//     border-right: 1px solid ${props => props.theme.WeekView.border};
//     border-bottom: 1px solid ${props => props.theme.WeekView.border};
//     border-spacing: 0;
//   }
//
//   > tbody tr td.no_border {
//     border: none;
//   }
//
//   > tbody tr td.no_border:hover {
//     background-color: inherit;
//     cursor: inherit;
//   }
//
//   > tbody td span.week-hours {
//     line-height: 48px;
//   }
//
//   > tbody tr td:hover {
//     background-color: ${props => props.theme.WeekView.bg_hover};
//     cursor: pointer;
//   }
// `
//
// const InputContainer = styled.div`
//   display: inline-block;
//   margin-top: 12px;
// `
//
// const BtnContainer = styled.div`
//   display: inline-block;
//   float: left;
//   width: 64px;
//   height: 128px;
//   margin-top: 48px;
// `

const generateDateVariables = (date) => {
  const l_date = date
  let l_vars = { date: l_date, year: l_date.getFullYear(), month: l_date.getMonth() }
  l_vars._startOfWeek = l_date.getDate() - (l_date.getDay() === 0 ? 6 : l_date.getDay() - 1)
  l_vars._endOfWeek = l_vars._startOfWeek + 6
  l_vars.startOfWeek = new Date(l_date.getFullYear(), l_date.getMonth(), l_vars._startOfWeek)
  l_vars.endOfWeek = new Date(l_date.getFullYear(), l_date.getMonth(), l_vars._endOfWeek)
  l_vars.year = l_date.getFullYear()
  l_vars.month = l_date.getMonth()
  l_vars.days = [0, 1, 2, 3, 4, 5, 6].map(i =>
    new Date(l_vars.startOfWeek.getFullYear(), l_vars.startOfWeek.getMonth(), l_vars.startOfWeek.getDate() + i).getDate()
  )
  return l_vars
}

const changeRouteTo = (route, history, bool=true) => {
  if(bool) history.push(route)
}

export default class WeekView extends Component {
  constructor(props) {
    super(props)
    this.room_el = undefined

    this._dates = generateDateVariables(dates.parseDateInfo(this.props.match.params.date))

    this.state = Object.assign({
      popup: '',  // not really important data in case of URL change, can be reset
      open: false,
      data: this.getDefaultData(),
      close: () => this.setState({ open: false }),
      hours: [...Array(12).keys()].map(j => [...Array(7).keys()].map(j => ({
        reserved: false,
        person: undefined,
        room: undefined,
        title: undefined,
        item: undefined,
      })))
    }, this._dates)
    /* eslint ignore-next */ // this does not count as 'fixing' something, will be done later
    this.state.hours = this.fetchData()
  }

  updateDateState(dateStr) {
    this.setState(generateDateVariables(dates.parseDateInfo(dateStr || this.props.match.params.date)))
  }

  updateEventState() {
    this.setState({
      hours: this.fetchData()
    })
  }

  updateDefaultData(dateStr) {
    this.setState({data: this.getDefaultData(dateStr)})
  }

  getDefaultData(dateStr) {
    return {
      date: dates.stringifyDateInfoPadded(
        dates._parseDateInfo(dateStr) || (this.state !== undefined ? this.state : this._dates).startOfWeek
      )
    }
  }

  fetchData() {
    let hours = [...Array(12).keys()].map(j => [...Array(7).keys()].map(j => ({
      reserved: false,
      person: undefined,
      room: undefined,
      title: undefined,
      item: undefined,
    })))

    hours[1][4] = {
      reserved: true,
      person: 'BRA',
      room: 123,
      title: '',
      item: 1,
    }

    return hours
  }

  componentWillReceiveProps(nextProps) {
    this.updateDateState(nextProps.match.params.date)
    this.updateDefaultData()
  }

  render() {
    return (
      <WeekViewWrapper className="week-view">
        <PopupWrapper open={this.state.open} popup={this.state.popup} data={this.state.data} close={this.state.close}/>
        <TitleWrapper>
          <TitleMonth>{dates.months[this.state.month]}</TitleMonth>
          <TitleYear>{this.state.year}</TitleYear>
          <TitleWeek>{this.state.startOfWeek.getDate() + ' - ' + this.state.endOfWeek.getDate()}</TitleWeek>
        </TitleWrapper>
        <Table id="weektable">
          <tbody>
            <tr>
            <th>Std</th>
              {this.state.days.map((day, i) => (
                <th tabIndex={0} onClick={() => changeRouteTo(`/interface/day/${dates.stringifyDateInfo(this.state.startOfWeek, 0, i)}`, this.props.history)} key={i}>
                  {dates.week_days_short[i] + " " + day}
                </th>
              ))}
            </tr>
            {this.state.hours.map((hour, j) => (
              <tr key={j}>
                <td key={j + '.' + 0} className="no_border">{j+1}</td>
                {hour.map((event, k) =>
                  event.reserved
                    ? <WeekViewItem key={j + "." + k+1} event={event} onUse={() => this.setState({popup: 'event', open: true, data: event})} />
                    : <td key={j + "." + k+1} />
                )}
              </tr>
            ))}
          </tbody>
        </Table>
        <BtnContainer>
          <NavButton changeRoute={changeRouteTo.bind(this,
            `/interface/week/${dates.stringifyDateInfo(this.state.date, 0, -7)}`, this.props.history)}>
            arrow_back
          </NavButton>
          <NavButton changeRoute={changeRouteTo.bind(this,
            `/interface/week/${dates.stringifyDateInfo(this.state.date, 0, +7)}`, this.props.history)}>
            arrow_forward
          </NavButton>
          <FAB role="button" tabIndex={0} className="btn"
               onKeyPress={e => aria.spacebar(e, () => this.setState({popup: 'add', open: true}))}
               onClick={() => this.setState({popup: 'add', open: true})}>
            <i className="material-icons">add</i>
          </FAB>
          <InputContainer>
            <Select aria-label="Laptop-wagen Auswahl (nach Raum)" innerRef={input => this.room_el = input} style={{margin: '4px'}} defaultValue="R111">
              <option value="R111">R111</option>
              <option value="R222">R222</option>
              <option value="R333">R333</option>
            </Select>
          </InputContainer>
        </BtnContainer>
      </WeekViewWrapper>
    )
  }
}
