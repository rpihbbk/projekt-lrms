import React from 'react'
import pt from 'prop-types'

const Json = ({ json }) => (
  <div style={{textAlign: "left"}}>
    {JSON.stringify(json, 2, 2).replace(/ /g, '\u00A0').split('\n').map((str, i) => (<span key={i}>{str}<br /></span>))}
  </div>
)

Json.propTypes = {
  json: pt.oneOfType([pt.object, pt.array])
}

export default Json
