import React from 'react'
import styled from 'styled-components'
import pt from 'prop-types'

import NamelookupWrapper from './namelookupWrapper.js'

import aria from '../util/aria-support-helpers.js'

const ItemWrapper = styled.td`
  box-sizing: border-box;
  text-align: left;
  padding: 0px;
  /*background-color: ${props => props.theme.DayView.reserved};*/

`

const PersonComponent = ({ className, children }) => (
  <span className={className}>
    Lehrer: <span>{children}</span>
  </span>
)

const Person = styled(PersonComponent)`
  display: inline-block;
  font-size: 13px;
  padding: 10px 0px 0px 8px;

  > span {
    font-size: 15px;
  }
`

const RoomComponent = ({ className, children }) => (
  <span className={className}>
    Raum: <span>{children}</span>
  </span>
)

const Room = styled(RoomComponent)`
  display: inline-block;
  font-size: 13px;
  padding: 11px 0px 0px 8px;

  > span {
    font-size: 15px;
  }
`

const EditWrapper = styled.div`
  display: inline-block;
  float: right;
  background-color: ${props => props.theme.DayView.reserved};
  padding: 5px 4px 6px 5px;
  outline: none;

  > i {
    font-size: 18px;
    padding: 6px 6px 6px 6px;
    border-radius: 50%;
  }

  :focus, :hover {
    background-color: ${props => props.theme.DayView.bg_hover};
  }

  > i:hover, :focus > i {
    background-color: rgba(0, 0, 0, 0.15);
  }
`

const InfoWrapper = styled.div`
  display: inline-block;
  height: 41px;
  width: ${props => props.full_width ? '100%' : 'calc(100% - 39px)'};
  background-color: ${props => props.theme.DayView.reserved};
  outline: none;

  :focus, :hover {
    background-color: ${props => props.theme.DayView.bg_hover};
  }
`

const Edit = ({ onUse, disabled }) =>
  disabled ? null : (
      <EditWrapper tabIndex={0} onClick={onUse} onKeyPress={e => aria.spacebar(e, onUse)}>
        <i className="material-icons">edit</i>
      </EditWrapper>
  )

Edit.propTypes = {
  onUse:    pt.func,
  disabled: pt.bool
}


const DayViewItem = ({ event, canEdit, onUse, onEdit, isPersonId=true }) => (
  <ItemWrapper className="reserved">
    <InfoWrapper full_width={!canEdit} role="button" tabIndex={onUse ? 0 : -1} onClick={onUse} onKeyPress={e => aria.spacebar(e, onUse)}>
      <Person>{isPersonId ? <NamelookupWrapper id={event.person} /> : event.person.substring(0, 12)}</Person>
      <Room>{event.room}</Room>
    </InfoWrapper>
    <Edit onUse={onEdit} disabled={!canEdit} />
  </ItemWrapper>
)

DayViewItem.propTypes = {
  isPersonId: pt.bool,
  canEdit:    pt.bool,
  onUse:      pt.func,
  onEdit:     pt.func,
  event:      pt.shape({
    title:      pt.string,
    person:     pt.string,
    room:       pt.number,
    item:       pt.number,
    reserved:   pt.bool
  })
}

export default DayViewItem
