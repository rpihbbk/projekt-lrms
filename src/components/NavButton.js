import React from 'react'
import pt from 'prop-types'
import aria from '../util/aria-support-helpers.js'
import Span from './FAB.js'

const NavButton = ({changeRoute, children}) => (
  <Span role="button" tabIndex={0} className="btn"
   onKeyPress={e => aria.spacebar(e, () => changeRoute())}
   onClick={e => changeRoute()}>
    <i className="material-icons">{children}</i>
  </Span>
)

NavButton.propTypes = {
  changeRoute:  pt.func
}

export default NavButton
