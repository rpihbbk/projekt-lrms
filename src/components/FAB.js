import styled from 'styled-components'

export default styled.span`
  display: inline-block;
  width: 48px;
  height: 48px;
  margin: 8px;
  user-select: none;
  color: ${props => props.theme.NavButton.color};
  border-radius: 50%;
  background-color: ${props => props.theme.NavButton.bg1};
  cursor: pointer;
  box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);

  > :focus {
    background-color: ${props => props.theme.NavButton.bg2};
    box-shadow: 0 8px 10px 1px rgba(0,0,0,0.14), 0 3px 14px 2px rgba(0,0,0,0.12), 0 5px 5px -3px rgba(0,0,0,0.3);
  }

  > i {
    margin-top: 12px;
  }
`
