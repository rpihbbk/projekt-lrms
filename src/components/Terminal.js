import React, { Component } from 'react'

import { parser } from '../util/Terminal.js'
import '../css/Terminal.css'


let UP_CHAR_CODE = 38
let DOWN_CHAR_CODE = 40

export default class Terminal extends Component {
  constructor() {
    super()

    this.history = [{text: 'Welcome\n', user: false, error: false}]
    this.commands = []
    this.i = 0
    this.prefix = '~ '
    this.current_input = ''
    this.parse = this.parse.bind(this)
    this.print = this.print.bind(this)
    this.println = this.println.bind(this)
    this.keyUp = this.keyUp.bind(this)
    this.renderHistoryItem = this.renderHistoryItem.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.getPrintHistory = this.getPrintHistory.bind(this)
    this.getCommandHistory = this.getCommandHistory.bind(this)
    this.setPrintHistory = this.setPrintHistory.bind(this)
    this.setCommandHistory = this.setCommandHistory.bind(this)
    this.setEnv = this.setEnv.bind(this)
    this.getEnv = this.getEnv.bind(this)
    this.delEnv = this.delEnv.bind(this)

    this.env = {
      print: this.print,
      println: this.println,
      getHistory: this.getPrintHistory,
      getCommands: this.getCommandHistory,
      setHistory: this.setPrintHistory,
      setCommands: this.setCommandHistory,
      setEnvVariable: this.setEnv,
      getEnvVariable: this.getEnv,
      delEnvVariable: this.delEnv,
      user: 'jannik',
      cwd: '/'
    }

    this.println('Type \'help\' for help')
  }

  componentDidMount() {
    this.refs.input.focus();
  }

  componentDidUpdate() {
    this.refs.input.scrollIntoView(false)
  }

  keyUp(evt) {
    if(evt.which === UP_CHAR_CODE) {
      if(this.commands[this.i - 1]) {
        if(this.i === this.commands.length) {
          this.current_input = this.refs.input.value
        }
        this.refs.input.value = this.commands[--this.i]
      }
    } else if(evt.which === DOWN_CHAR_CODE) {
      if(this.commands[this.i + 1]) {
        this.refs.input.value = this.commands[++this.i]
      } else {
        this.i = this.commands.length
        this.refs.input.value = this.current_input
      }
    }
  }

  parse(command) {
    parser(command, this.env)
  }

  handleSubmit(evt) {
    let l_value = this.refs.input.value
    evt.preventDefault()
    this.history.push({text: l_value + '\n', user: true, error: false})
    this.commands.push(l_value)
    this.i = this.commands.length
    this.parse(l_value)
    this.forceUpdate()
    this.refs.input.value = ''
  }

  renderHistoryItem(item, i) {
    return (
      <span key={i}>
        {item.user ? <span className="prefix">{this.prefix}</span> : ''}{item.text}{item.text.endsWith('\n') ? <br /> : ''}
      </span>
    )
  }

  print(string = '') {
    this.history.push(...string.split('\n').map((text, i, arr) =>
      ({
        text: text + (i === arr.length - 1 ? '' : '\n'),
        user: false,
        error: false
      })
    ))
  }

  println(string = '') {
    this.history.push(...string.split('\n').map(text =>
      ({
        text: text + '\n',
        user: false,
        error: false
      })
    ))
  }

  getPrintHistory() {
    return this.history
  }

  setPrintHistory(history) {
    this.history = history
  }

  getCommandHistory() {
    return this.commands
  }

  setCommandHistory(commands) {
    this.commands = commands
  }

  setEnv(key, value) {
    this.env[key] = value
  }

  getEnv(key) {
    return this.env[key]
  }

  delEnv(key) {
    delete this.env[key]
  }

  render() {
    return (
      <div className={'Terminal' + (this.props.headless ? ' headless' : '')}>
        <div className="header">
          <span className="circle circle-red" onClick={this.onClose}></span>
          <span className="circle circle-yellow" onClick={this.onMinimize}></span>
          <span className="circle circle-green" onClick={this.onExpand}></span>
        </div>
        <div className="body" onClick={() => this.refs.input.focus()}>
          {this.history.map(this.renderHistoryItem)}
          <form className="form" onSubmit={this.handleSubmit}>
            <span className="prefix">{this.prefix}</span>
              <input
                type="text"
                autoComplete="off"
                onKeyUp={this.keyUp}
                ref="input"
                className="input"
              />
          </form>
        </div>
      </div>
    )
  }
}
