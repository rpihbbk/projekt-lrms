import React from 'react'

//import logo from '../../public/logo.svg'

import styled from 'styled-components'

import '../css/Header.css'

const Header = styled.header`
background-color: ${props => props.theme.Header.bg};
`

const Title = styled.span`
color: ${props => props.theme.Header.color};
`

const ImgWrapper = styled.div`
  filter: ${props => props.theme.Header.filter}`

export default () => (
  <Header className="header" role="banner">
    <Title className="title">Hans-Böckler-Berufskolleg</Title>
    <ImgWrapper>
    <img src={'/logo.svg'} className="logo" alt="logo" />
    <img src={'/hbbk.png'} className="hbbk-logo" alt="hbbk" />
    </ImgWrapper>
  </Header>
)
