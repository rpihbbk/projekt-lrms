import React from 'react'
import styled from 'styled-components'
import pt from 'prop-types'

import storage from '../util/storage.js'
import fn from '../util/fn.js'
import { namelookup } from '../util/lookup.js'

import InputField from './InputField.js'
import Spoiler from './Spoiler.js'

const mapObj = fn.mapObj

const PopupWrapper = styled.div`
  text-align: left;
  padding: 0px 8px;
`

const PersonInput = ({defaultData, isPerson}) => {
  console.log(defaultData, isPerson)

  const checkLookup = (lookup, id) =>
    typeof(lookup) === 'string'
      ? lookup
      : 'id:' + id

  let str = defaultData.person
    ? isPerson
      ? defaultData.person
      : checkLookup(namelookup(defaultData.person), defaultData.person)
    : storage.getItem('user')

  console.log(str)
  return (
    <InputField
      obj_key={'person'}
      value={str}
      small={true}
      title="Lehrer: "
      disabled={storage.getItem('user') !== 'root'} />
  )
}
const ButtonContainer = styled.div``

const Button = styled.button``

const _getValues = (ref) => {
  let obj = {}
  Array.from(ref.querySelectorAll('.__value'))
    .forEach(node => {
      const classArray = Array.from(node.classList)
      const get = ((arr, str) => (x => x[0] ? x[0].substr(str.length) : '')(arr.filter(x => x.startsWith(str)))).bind(this, classArray)

      console.log(node)

      const l_obj = {
        type: get('type:') || 'text',
        val: get('val:'),
        key: get('key:'),
        has_hidden: get('has_hidden:'),
        computed_showing: get('computed_showing:'),
        disabled: get('disabled:')
      }

      console.log(l_obj)

           if(l_obj.type === 'number')    l_obj.value = +node.value
      else if(l_obj.type === 'checkbox')  l_obj.value = node.checked
      else if(l_obj.type === 'date')      l_obj.value = new Date(node.value).toISOString()
      else                                l_obj.value = node.value || node.innerText

      obj[classArray.filter(x => x.startsWith('key:'))[0].substr(4)] = l_obj
    })
  return obj
}

const getValues = (ref) => mapObj(_getValues(ref), (val, key) => val.value)

let el = null

const AddPopup = ({ cb, close, del, isPerson=false, defaultData={} }) => {
  console.log(defaultData.person)

  return (
    <PopupWrapper innerRef={ref => el = ref}>
      <InputField obj_key={'title'} value={defaultData.title} small={true} title="Titel: " type="text" placeholder="Titel" /><br />
      {/*PersonInput({defaultData: defaultData, isPerson: isPerson}) */ null}<br />
      <PersonInput defaultData={defaultData} isPerson={isPerson} /><br />
      <InputField obj_key={'room'} value={defaultData.room} small={true} title="Raum: " type="number" placeholder="Raum" /><br />
      <InputField obj_key={'item'} value={defaultData.item} small={true} title="Laptop Wagen: " type="number" placeholder="Laptop Wagen" /><br />
      <InputField obj_key={'start'} value={defaultData.start} small={true} min={1} max={12} title="von" type="number" placeholder="1-12" /><br />
      <InputField obj_key={'end'} value={defaultData.end} small={true} min={1} max={12} title="bis" type="number" placeholder="1-12" /><br />
      <InputField obj_key={'date'} value={defaultData.date} small={true} title="Datum" type="date" /><br />
      <Spoiler open={false} title={'Wiederholend?'}>
        <InputField obj_key={'recurring'} value={defaultData.recurring} small={true} title="Wiederholend?" type="checkbox" /><br />
        <InputField obj_key={'recurring_time'} value={defaultData.recurring_time} small={true} title={'Wiederholungs-rate'} type="number" placeholder={'alle x Tage'} /><br />
      </Spoiler>
      <br />
      <ButtonContainer>
        <Button onClick={() => close(false)}>Cancel</Button>
        {del ? <Button onClick={del}>Delete</Button> : null}
        <Button onClick={() => (console.log(getValues(el)), cb(getValues(el)))}>OK</Button>
      </ButtonContainer>
    </PopupWrapper>
  )
}

AddPopup.propTypes = {
  cb:           pt.func,
  close:        pt.func,
  del:          pt.func,
  isPerson:     pt.bool,
  defaultData:  pt.shape({
    title:    pt.string,
    person:   pt.string,
    room:     pt.number,
    item:     pt.number,
    reserved: pt.bool,
    date:     pt.date,
    start:    pt.number,
    end:      pt.number
  }),
}

export default AddPopup
