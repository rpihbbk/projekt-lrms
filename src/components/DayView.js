// react imports
import React, { Component } from 'react'
// dates utility
import dates from '../util/dates.js'
// localStorage utility
import storage from '../util/storage.js'
// fetch json
import fetchJSON from '../util/fetchJSON.js'
// ARIA support functions
import aria from '../util/aria-support-helpers.js'
// converting from backend events to frontend events
import { convertDay } from '../util/convertFetch.js'

import fn from '../util/fn.js'

import { namelookup } from '../util/lookup.js'
// object method chaining
//import pipe from '../util/pipe.js'
// components
import NavButton from './NavButton.js'
import FAB from './FAB.js'
import DayViewItem from './DayViewItem.js'
import PopupWrapper from './PopupWrapper.js'
import { Select } from './Elements.js'
// styled components
import { DayViewWrapper, TitleWrapper, TitleMonth, TitleYear, TitleWeek, InputContainer, BtnContainer, Table } from './DayViewStyled.js'

const pass = fn.pass

// date variables
const generateDateVariables = (date) => {
  const l_date = date
  let l_vars = {
    date: l_date,
    year: l_date.getFullYear(),
    month: l_date.getMonth()
  }
  return l_vars
}

// route changing
const changeRouteTo = (route, history, bool=true) => {
  if(bool) history.push(route)
}

export default class DayView extends Component {
  constructor(props) {
    super(props)

    this.state = this.generateState(dates.parseDateInfo(this.props.match.params.date))
  }

  generateEmptyHourState() {
    return [...Array(12).keys()].map(j => ({
      reserved: false,
      person: undefined,
      room: undefined,
      title: undefined,
      item: undefined
    }))
  }

  generateState(date) {
    const date_var = generateDateVariables(date)
    return {
      date: date_var.date,
      year: date_var.year,
      month: date_var.month,
      hours: this.generateEmptyHourState(),
      popup: '',
      open: false,
      data: dates.stringifyDateInfoPadded(date_var.date),
      close: (shouldUpdate=false) => {
        console.log('triggered close' + (shouldUpdate ? ' and should update (shouldUpdate=' + shouldUpdate + ')' : ''))
        this.setState({ open: false })

        if(shouldUpdate)
          this.fetch(dates.parseDateInfo(this.props.match.params.date))
      }
    }
  }

  fetch(date) {
    //console.log(this.state.hours)
    return fetchJSON(window.BACKEND + '/event/get', {
      date: {
        end:    dates.setTimeIgnoreTimezone(this.state.date, 23, 59, 59, 999).toISOString(),  // end first because i want date to
        start:  dates.setTimeIgnoreTimezone(this.state.date, 0, 0, 0, 0).toISOString()        // be the start of the day in state
      }
    }, 'POST')
      .then(res => convertDay(res))
      .then(pass(res => Array.from(new Set(
        res
          .filter(x => x.reserved)
          .map(x => x.person)
        ))
        .forEach(x => namelookup(x))
      ))
      .then(res => this.setState({hours: res}))
      //.then(() => console.log(this.state.hours))
      //.then(() => console.log(dates.setTimeIgnoreTimezone(this.state.date, 23, 59, 59, 999).toISOString()))
      //.then(() => console.log(dates.setTimeIgnoreTimezone(this.state.date, 0, 0, 0, 0).toISOString()))
  }

  componentWillReceiveProps(nextProps) {
    const date = dates.parseDateInfo(nextProps.match.params.date)
    this.setState(this.generateState(date), () => this.fetch(date))
  }

  componentWillMount() {
    this.fetch(dates.parseDateInfo(this.props.match.params.date))
  }

  render() {
    return (
      <DayViewWrapper className="day-view">
        <PopupWrapper open={this.state.open} popup={this.state.popup} data={this.state.data} close={this.state.close}/>
        <TitleWrapper>
          <TitleMonth>{dates.months[this.state.month]}</TitleMonth>
          <TitleYear>{this.state.year}</TitleYear>
          <TitleWeek>{this.state.date.getDate()}</TitleWeek>
        </TitleWrapper>
        <Table id="daytable">
          <tbody>
            <tr>
            <th>Std</th>
              <th className="timetable">{dates.week_days_short[this.state.date.getDay() === 0 ? 6 : this.state.date.getDay() - 1] + " " + this.state.date.getDate()}</th>
            </tr>
            {this.state.hours.map((hour, j) => (
              <tr key={j}>
                <td className="no_border">{j+1}</td>
                {hour.reserved
                  ? <DayViewItem
                      event={hour}
                      canEdit={hour.person === storage.getItem('userId') || storage.getItem('user') === 'root'}
                      onUse={() => {console.log('show');this.setState({popup: 'event', open: true, data: hour})}}
                      onEdit={() => {console.log('edit');this.setState({popup: 'modify', open: true, data: hour})}}
                    />
                  : <td className={'timetable'}></td>
                }
              </tr>
            ))}
          </tbody>
        </Table>
        <BtnContainer>
          <NavButton changeRoute={changeRouteTo.bind(this,
            `/interface/day/${dates.stringifyDateInfo(this.state.date, 0, -1)}`, this.props.history)}>
            arrow_back
          </NavButton>
          <NavButton changeRoute={changeRouteTo.bind(this,
            `/interface/day/${dates.stringifyDateInfo(this.state.date, 0, +1)}`, this.props.history)}>
            arrow_forward
          </NavButton>
          <FAB role="button" tabIndex={0} className="btn"
               onKeyPress={e => aria.spacebar(e, () => this.setState({popup: 'add', open: true, data: {date: dates.stringifyDateInfoPadded(this.state.date, 0, 0)}}))}
               onClick={() => this.setState({popup: 'add', open: true, data: {date: dates.stringifyDateInfoPadded(this.state.date, 0, 0)}})}>
            <i className="material-icons">add</i>
          </FAB>
          <InputContainer>
            <Select aria-label="Laptop-wagen Auswahl (nach Raum)" innerRef={input => this.room_el = input} style={{margin: '4px'}} defaultValue="R111">
              <option value="R111">R111</option>
              <option value="R222">R222</option>
              <option value="R333">R333</option>
            </Select>
          </InputContainer>
        </BtnContainer>
      </DayViewWrapper>
    )
  }
}
