import React, { Component } from 'react'
import styled from 'styled-components'

import default_settings from '../util/default_settings.js'

import storage from '../util/storage.js'

import { Title, TitleSmall, Span, Hr, Select } from './Elements.js'

const SettingsWrapper = styled.div`
  text-align: left;
  padding-right: calc(16px + 5%);
  padding-left: 16px;
  padding-bottom: 24px;
`

export default class Settings extends Component {
  constructor(props) {
    super(props)
    this.load()

    this.el = {
      select: null
    }

    this.save_to_settings = this.save_to_settings.bind(this)

  }

  save() {
    storage.json.setItem(storage.getItem('user'), this.settings, 'settings')
  }

  load() {
    this.settings = storage.json.getItem(storage.getItem('user'))
  }

  reset() {
    this.settings = default_settings()
    this.save()
  }

  save_to_settings(el, property) {
    this.settings[property] = el.value
    this.save() // still need to somehow swap that className attribute value on Main... (this.forceUpdate() does not work)
    this.forceUpdate() // forceUpdate just somehow enables being able to change the value.. don't know why but it does not seem to work otherwise
  }

  render() {
    return (
      <SettingsWrapper>
        <Title>Einstellungen</Title>
        <Hr />

        <TitleSmall>Aussehen:</TitleSmall>
        <Span>
          Farbschema:
          <Select onChange={() => this.save_to_settings(this.el.select, 'theme')}
                  innerRef={input => this.el.select = input} style={{marginLeft: '12px'}}
                  value={this.settings.theme}>
            <option value="default">Normal</option>
            <option value="light">Hell</option>
            <option value="dark">Dunkel</option>
          </Select>
        </Span>
      </SettingsWrapper>
    )
  }
}
