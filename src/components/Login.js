import React, { Component } from 'react'
import '../css/Login.css'

import aria from '../util/aria-support-helpers.js'
import default_settings from '../util/default_settings.js'
import storage from '../util/storage.js'
import { idlookup } from '../util/lookup.js'

import InputField from './InputField'

export default class Login extends Component {
  constructor() {
    super()

    this.login = this.login.bind(this)

    storage.setItem('jwt', '', 'jwt')
    storage.setItem('user', '', 'user')
    storage.setItem('userId', '', 'user')

    this.username_field = {
      error: '',
      type: 'text',
      placeholder: 'Benutzername',
      title: 'Benutzername:',
      cb: el => {
        this.username_field.el = el
      }
    }
    this.password_field = {
      error: '',
      type: 'password',
      placeholder: 'Passwort',
      title: 'Passwort:',
      cb: el => {
        this.password_field.el = el
      }
    }
  }

  login() {
    let username = this.username_field.el.value
    let password = this.password_field.el.value
    fetch(window.BACKEND + '/auth', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ username: username, password: password })
    })
      .then(res => res.status === 401 ? null : res.text())
      .then(text => {
        if(text === null) return
        else {
          storage.setItem('jwt', text, 'jwt')
          storage.setItem('user', username, 'user')

          const lookup = idlookup(username)
          typeof(lookup) === 'string' ? storage.setItem('userId', lookup) : lookup.then(id => storage.setItem('userId', id))

          // ((lookup, set) =>
          //   typeof(lookup) === 'string'
          //     ? set(lookup)
          //     : lookup.then(id => set(id))
          // )(idlookup(username), x => storage.setItem('userId', x))

          if(!storage.getItem(username)) {
            storage.json.setItem(username, default_settings(), 'settings')
          }
          console.log(text)
          this.props.history.push('/interface')
        }
      })
  }

  render() {
    return (
      <div className="Login">
        <div className="login-area">
          <InputField type={this.username_field.type} title={this.username_field.title} placeholder={this.username_field.placeholder} error={this.username_field.error} cb={this.username_field.cb} />
          <InputField type={this.password_field.type} title={this.password_field.title} placeholder={this.password_field.placeholder} error={this.password_field.error} cb={this.password_field.cb} />
          <span tabIndex={0} role="button" onKeyPress={e => aria.spacebar(e, this.login)} className="login-btn" onClick={this.login}>Login</span>
        </div>
      </div>
    )
  }
}
