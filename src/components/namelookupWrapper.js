import React, { Component } from 'react'
import pt from 'prop-types'

import { namelookup } from '../util/lookup'

class NamelookupWrapper extends Component {
  constructor(props) {
    super(props)
    const lookup = namelookup(props.id)

    this.state = {
      id: props.id,
      name: typeof(lookup) === 'string' ? lookup : null,
      fn: typeof(lookup) !== 'string' ? lookup : null,
     }
  }

  componentDidMount() {
    if(!this.state.name)
      this.state.fn.then(name => this.setState({name: name, fn: null}))
  }

  render() {
    return (
      <span>{this.state.name || (this.props.idFallback ? this.state.id : this.props.fallbackString) || '...'}</span>
    )
  }
}

NamelookupWrapper.propTypes = {
  id:             pt.string,
  idFallback:     pt.bool,
  fallbackString: pt.string
}

export default NamelookupWrapper
