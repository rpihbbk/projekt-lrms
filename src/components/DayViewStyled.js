import styled from 'styled-components'

const DayViewWrapper = styled.div`
  padding: 8px;
  float: left;
`

const TitleWrapper = styled.div`
  text-align: left;
  margin-left: 12px;
`

const TitleMonth = styled.span`
  font-size: 26px;
  font-weight: 600;
`

const TitleYear = styled.span`
  margin: 0px 12px 0px 6px;
  font-size: 26px;
`

const TitleWeek = styled.span`
  font-size: 16px;
`

const InputContainer = styled.div`
  display: inline-block;
  margin-top: 12px;
`

const BtnContainer = styled.div`
  display: inline-block;
  float: left;
  width: 64px;
  height: 128px;
  margin-top: 48px;
`

const Table = styled.table`
  display: inline-block;
  float: left;
  border-spacing: 0;

  > tbody tr th {
    width: 48px;
    height: 48px !important;
    border-bottom: 2px solid ${props => props.theme.DayView.border};
  }

  > tbody tr td {
    width: 336px; // why was this set to 48px? the 48px are now being set in the .no_border class instead of here
    height: 40px !important;
    border: 0px;
    border-right: 1px solid ${props => props.theme.DayView.border};
    border-bottom: 1px solid ${props => props.theme.DayView.border};
    border-spacing: 0;
  }

  > tbody tr th.timetable {
    padding-left: 8px;
    text-align: left;
  }

  > tbody tr td.timetable {
    padding-left: 8px; // what does this actually change? it seems this is applied on the wrong selector
    text-align: left;  // it only applies to td's which are not reserved, and settings text-align: left on
    width: 336px;      // on those doesn't really make sense.
  }

  > tbody tr td.no_border {
    border: none;
    width: 48px;
  }

  > tbody tr td.no_border:hover {
    background-color: inherit;
    cursor: inherit;
  }

  > tbody td span.week-hours {
    line-height: 48px;
  }

  > tbody tr td:hover {
    background-color: ${props => props.theme.DayView.bg_hover};
    cursor: pointer;
  }
`

export { DayViewWrapper, TitleWrapper, TitleMonth, TitleYear, TitleWeek, InputContainer, BtnContainer, Table }
