import React from 'react'
import styled from 'styled-components'
import pt from 'prop-types'

import NamelookupWrapper from './namelookupWrapper'
import ItemlookupWrapper from './itemlookupWrapper'

import InputField from './InputField.js'

const PopupWrapper = styled.div`
  text-align: left;
  padding: 0px 8px;
`

const ButtonContainer = styled.div`

`

const Button = styled.button`

`


const InfoPopup = ({ close, event }) => (
  <PopupWrapper>
    <InputField small={true} title="Titel: " disabled={true}>{event.title || '/'}</InputField><br />
    <InputField small={true} title="Lehrer: " disabled={true}><NamelookupWrapper id={event.person} idFallback={true} fallbackString={'/'} /></InputField><br />
    <InputField small={true} title="Raum: " disabled={true}>{event.room || '/'}</InputField><br />
    <InputField small={true} title="Laptop-Wagen: " disabled={true}><ItemlookupWrapper id={event.item} idFallback={true} fallbackString={'/'} /></InputField><br />
    <InputField small={true} title="Datum: " disabled={true}>{event.date}</InputField><br />
    <ButtonContainer>
      <Button onClick={() => close(false)}>Close</Button>
    </ButtonContainer>
  </PopupWrapper>
)

InfoPopup.propTypes = {
  close: pt.func,
  event: pt.shape({
    title:    pt.string,
    person:   pt.string,
    room:     pt.number,
    item:     pt.number,
    date:     pt.date,
    reserved: pt.bool
  })
}

export default InfoPopup
