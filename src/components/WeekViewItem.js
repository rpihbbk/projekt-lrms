import React from 'react'
import styled from 'styled-components'
import pt from 'prop-types'

import NamelookupWrapper from './namelookupWrapper.js'

import aria from '../util/aria-support-helpers.js'

const ItemWrapper = styled.td`
  background-color: ${props => props.theme.WeekView.reserved};
  font-size: 14px;
`

const WeekViewItem = ({ onUse, event, isPersonId=true }) => (
  <ItemWrapper className="reserved" role="button" tabIndex={onUse ? 0 : -1} onClick={onUse} onKeyPress={e => aria.spacebar(e, onUse)}>
    {isPersonId ? <NamelookupWrapper id={event.person} /> : event.person.substring(0, 6)}
  </ItemWrapper>
)

WeekViewItem.propTypes = {
  isPersonId: pt.bool,
  onUse:      pt.func,
  event:      pt.shape({
    title:      pt.string,
    person:     pt.string,
    room:       pt.number,
    item:       pt.number,
    reserved:   pt.bool
  })
}

export default WeekViewItem
