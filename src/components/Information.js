import React from 'react'
import { Link } from 'react-router-dom'

import { Title, TitleSmall, Ul, Li, Span, Hr } from './Elements.js'

import styled from 'styled-components'

const InformationWrapper = styled.div`
  text-align: left;
  padding-right: calc(16px + 5%);
  padding-left: 16px;
  padding-bottom: 24px;
`


export default props => (
  <InformationWrapper className="information">
    <Title className="title">Informationen</Title>
    <Hr />

    <TitleSmall className="title-small">Entwickelt von:</TitleSmall>
    <Span>
      <Ul>
        <Li>Jannik Wibker</Li>
        <Li>Paul Brauckmann</Li>
      </Ul>
    </Span>
    <TitleSmall className="title-small">Designed von:</TitleSmall>
    <Span>
      <Ul>
        <Li>Anna Uphoff</Li>
        <Li>Sailaxman Muraleetharan</Li>
      </Ul>
    </Span>
    <TitleSmall className="title-small">Programmiersprachen:</TitleSmall>
    <Span>
      <Ul>
        <Li>javascript</Li>
        <Li>css</Li>
        <Li>html5</Li>
      </Ul>
    </Span>
    <TitleSmall className="title-small">wir benutzen:</TitleSmall>
    <Span>
      <Ul>
        <Li>react</Li>
        <Li>react router</Li>
        <Li>styled-components</Li>
        <Li>skeleton css</Li>
        <Li>express</Li>
        <Li>jwt</Li>
        <Li>neDB</Li>
      </Ul>
      <p>Alle benutzten packages (<Link to="/interface/information/packages">package.json</Link>) für das frontend</p>
    </Span>
  </InformationWrapper>
)
