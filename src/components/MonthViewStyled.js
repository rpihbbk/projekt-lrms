import styled from 'styled-components'

const MonthViewWrapper = styled.div`
  display: flex;
`

const Table = styled.table`
  margin: 0px 8px;
  border-spacing: 0;
  width: calc(7 * 48px);

  > tbody tr th {
    width: 48px;
    height: 48px;
  }

  > tbody tr td {
    width: 48px;
    height: 48px;
    border: 0px;
    border-right: 1px solid rgba(0, 0, 0, 0.2);
    border-bottom: 1px solid rgba(0, 0, 0, 0.2);
    border-spacing: 0;
  }

  > tbody tr td:hover, tbody tr td:focus {
    background-color: rgba(162, 230, 249, 0.5);
    cursor: pointer;
  }

  > tbody tr td.empty:hover, tbody tr td.empty:focus {
    background-color: #fff;
    cursor: inherit;
    outline: none;
  }

  > tbody tr td:first-child {
    border-left: 1px solid rgba(0, 0, 0, 0.2);
  }

  > tbody tr:nth-child(2) td {
    border-top: 1px solid rgba(0, 0, 0, 0.2);
  }
`

const BtnContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 64px;
  height: 128px;
  margin-top: 48px;
`

export { MonthViewWrapper, Table, BtnContainer }
