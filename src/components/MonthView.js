import React from 'react'
//import styled from 'styled-components'

import dates from '../util/dates.js'
import aria from '../util/aria-support-helpers.js'

import NavButton from './NavButton.js'

import '../css/MonthView.css'

import { MonthViewWrapper, Table, BtnContainer } from './MonthViewStyled.js'

const generateDateVariables = date => {
  const l_date = date
  let l_vars = {
    date: l_date,
    ...dates.calculateWeeks(date)
  }
  return l_vars
}

const changeRouteTo = (route, history, bool=true) =>
  bool ? history.push(route) : null


export default ({ history, match }) => {
  let date = generateDateVariables(dates.parseDateInfo(match.params.date))
  return (
    <MonthViewWrapper className="MonthView">
      <Table>
        <tbody>
          <tr>
            <th>M</th>
            <th>D</th>
            <th>M</th>
            <th>D</th>
            <th>F</th>
            <th>S</th>
            <th>S</th>
          </tr>
          {date.weeks.map((item, i) => (
            <tr key={i}>
            {item.map((item, j) => (
              <td key={j} className={item !== 0 ? '' : 'empty'} tabIndex={item !== 0 ? 0 : -1}
                  onKeyPress={e => aria.spacebar(e, () => changeRouteTo('/interface/day/' + dates.stringifyDateInfo(item || undefined, 0), history, item !== 0))}
                  onClick={() => changeRouteTo('/interface/day/' + dates.stringifyDateInfo(item || undefined, 0), history, item !== 0)}>
                  {item !== 0 ? (
                    <span style={{fontSize: 12}}>
                      {item.getDate()}
                    </span>
                  ) : ''}
              </td>
            ))}
            </tr>
          ))}
        </tbody>
      </Table>
      <BtnContainer>
        <NavButton changeRoute={changeRouteTo.bind(this,
          `/interface/month/${dates.stringifyDateInfo(date.date, -1, 0)}`, history)}>
          arrow_back
        </NavButton>
        <NavButton changeRoute={changeRouteTo.bind(this,
          `/interface/month/${dates.stringifyDateInfo(date.date, +1, 0)}`, history)}>
          arrow_forward
        </NavButton>
      </BtnContainer>
    </MonthViewWrapper>
  )
}
