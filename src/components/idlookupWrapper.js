import React, { Component } from 'react'
import pt from 'prop-types'

import { idlookup } from '../util/lookup'

class IdlookupWrapper extends Component {
  constructor(props) {
    super(props)
    const lookup = idlookup(props.name)

    this.state = {
      name: props.name,
      id: typeof(lookup) === 'string' ? lookup : null,
      fn: typeof(lookup) !== 'string' ? lookup : null,
     }
  }

  componentDidMount() {
    if(!this.state.id)
      this.state.fn.then(id => this.setState({id: id, fn: null}))
  }

  render() {
    return (
      <span>{this.state.id || (this.props.nameFallback ? this.state.name : this.props.fallbackString) || '...'}</span>
    )
  }
}

IdlookupWrapper.propTypes = {
  name:           pt.string,
  nameFallback:   pt.bool,
  fallbackString: pt.string
}

export default IdlookupWrapper
