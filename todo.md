### TODO list

##### Things can be added freely to this list

(Things which are done should be marked as done (using [x] (replacing the space with an x at the beginning of the line)))

- [x] outline on Select (notice the capital 'S' (**Elements.js** components))
- [x] add button to add events (**Views**)
- [x] add popup for adding events (or editing them later)
- [x] make dayView full width (with special information such as events (with teacher names and such showing))
- [x] fix routing not updating routes if same component
- [x] fix this annoying popup system
- [x] make theme changes apply instantly
- [x] add events and items to the backend
- [x] **way** better error handling in the backend
- [x] add time to events and make events be able to last multiple hours
- [x] add GUI for recurring events
- [x] start fetching data (even if sample data) from backend
- [x] implement fetching event data for specified timeframe (such as for a whole month) in the backend
- [x] add name lookup for user ID's
- [x] reverse namelookup for adding / modifying events
- [x] fix adding events (idlookup / namelookup, ...)
- [x] fix permission problem (probably related to username / id thingy)
- [x] fix Popup not updating values
- [x] fix server not updating events on modification
- [x] update view on deletion / modification / addition of event
- [x] fix modifying events (showing id instead of username)
- [x] use idlookup / namelookup for displaying, adding and modifying events
- [x] fix visual bug when having 12 reserved items in 1 day (only .reserved and not one .timetable (this is what caused the bug (which was that the width was set to 48px instead of 336px)))
- [ ] disable / finish console / database / ... features
- [ ] check against overlapping events
- [ ] more robust python display script
- [ ] add unit tests
- [ ] make deployment easier (automation and such (maybe docker containers))
- [ ] easier network configuration (`nano build/index.html ctrl-W localhost` is not good)
- [ ] **add unit tests!!!**
- [ ] add required fields to items in backend (add_item.js and mod_item.js) (*should be really simple*)
- [ ] find a good system for recurring events (which does not pollute the database with a whole lot of entries)
- [ ] add media queries for MonthView, WeekView and DayView (3 each)
- [ ] use cache control headers
